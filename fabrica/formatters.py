# -*- coding: utf-8 -*-

def status_pedido(status):
    label, css = status
    return '<span class="label label-%s m-left-xs">%s</span>' % (css, label)


def status_venda(status):
    i, label = status
    css = ('info', 'warning', 'warning', 'success')[i]
    return '<span class="label label-%s m-left-xs">%s</span>' % (css, label)
