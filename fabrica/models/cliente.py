# -*- coding: utf-8 -*-

from djangoplus.db import models
from djangoplus.admin.models import Organization
from enderecos.models import Endereco
from fabrica.models.sistemico import Funcionario
from djangoplus.decorators import role


class Cliente(Organization):
    cnpj = models.CnpjField(verbose_name='CNPJ', null=True, blank=True)
    razao_social = models.CharField(verbose_name='Razão Social', search=True, null=True)
    nome = models.CharField(verbose_name='Nome Fantasia', search=True)
    ie = models.CharField(verbose_name='Inscrição Estadual', null=True, blank=True)

    telefone_principal = models.PhoneField(verbose_name='Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name='Telefone Secundário', null=True, blank=True)
    email = models.EmailField(verbose_name='E-mail', blank=True, null=True)

    endereco = models.OneToOneField(Endereco, verbose_name='Endereço', exclude=False, null=True, blank=True)

    token = models.CharField(verbose_name='Token', null=True, blank=True, exclude=True)

    filial_da_fabrica = models.BooleanField('Cliente da Fábrica', default=False, help_text='Marque essa opção caso o cliente seja uma filial da fábrica.')

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        menu = 'Sistêmico::Empresas', 'fa-exchange'
        can_admin = 'Administrador Sistêmico'
        icon = 'fa-compress'
        add_form = 'ClienteForm'
        usecase = 6

    fieldsets = (
        ('Dados Gerais', {'fields': (('cnpj', 'razao_social'), ('nome', 'ie'), 'token')}),
        ('Contato::Telefones/E-mail', {'fields': (('telefone_principal', 'telefone_secundario'), 'email')}),
        ('Contato::Endereço', {'relations': ('endereco',)}),
        ('Administração::Administradores', {'relations': ('administrador_set',)}),
        ('Lojas::Lojas', {'relations': ('loja_set',)}),
        ('Configuração::Configuração', {'fields': ('filial_da_fabrica',)}),
    )

    def __str__(self):
        return self.nome


class FuncionarioCliente(Funcionario):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente')

    class Meta:
        verbose_name = 'Funcionário'
        verbose_name_plural = 'Funcionários'
        abstract = True

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', ('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )


@role('cpf', scope='cliente')
class Administrador(FuncionarioCliente):
    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        can_admin = 'Administrador Sistêmico'
        list_lookups = 'cliente'
        menu = 'Sistêmico::Usuários::Administradores', 'fa-th'
        usecase = 7


@role('cpf', scope='cliente')
class GerenteEscritorio(FuncionarioCliente):
    class Meta:
        verbose_name = 'Gerente de Escritório'
        verbose_name_plural = 'Gerentes de Escritório'
        can_admin_by_organization = 'Administrador'
        list_lookups = 'cliente'
        menu = 'Sistêmico::Usuários::Gerentes de Escritório', 'fa-th'
        usecase = 8

