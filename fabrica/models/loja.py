# -*- coding: utf-8 -*-

from decimal import Decimal
from django.db.models.aggregates import Sum
from djangoplus.db import models
from djangoplus.decorators import action, meta, role
from djangoplus.admin.models import Unit
import datetime
import hashlib
from fabrica.models.sistemico import Funcionario


class Loja(Unit):

    cnpj = models.CnpjField(verbose_name='CNPJ', null=True, blank=True)
    razao_social = models.CharField(verbose_name='Razão Social', search=True, null=True)
    nome = models.CharField(verbose_name='Nome Fantasia', search=True)
    ie = models.CharField(verbose_name='Inscrição Estadual', null=True, blank=True)
    matriz = models.BooleanField(verbose_name='Matriz', default=False, help_text='Marque essa opção caso a loja seja a matriz')

    telefone_principal = models.PhoneField(verbose_name='Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name='Telefone Secundário', null=True, blank=True)
    email = models.EmailField(verbose_name='E-mail', blank=True, null=True)

    endereco = models.OneToOneField('enderecos.Endereco', verbose_name='Endereço', exclude=False, null=True, blank=True)

    token = models.CharField(verbose_name='Token', null=True, blank=True, exclude=True)
    pode_enviar_pedido = models.BooleanField(verbose_name='Pode enviar pedido', default=False, help_text='Marque essa opção caso o gerent da loja possa enviar o pedido direto para a fábrica.')

    contas = models.OneToManyField('fabrica.ContaBancaria', verbose_name='Contas Bancárias', exclude=True)

    cliente = models.ForeignKey('fabrica.Cliente', verbose_name='Cliente')

    class Meta:
        verbose_name = 'Loja'
        verbose_name_plural = 'Lojas'
        can_admin_by_organization = 'Administrador'
        can_list_by_unit = 'Gerente de Loja'
        icon = 'fa-building'
        menu = 'Sistêmico::Lojas', 'fa-th'
        verbose_female = True
        list_shortcut = 'Administrador'
        list_lookups = 'cliente'
        usecase = 9

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', ('cnpj', 'razao_social'), ('nome', 'ie'), 'matriz')}),
        ('Contato::Telefones e E-mail', {'fields': (('telefone_principal', 'telefone_secundario'),'email')}),
        ('Contato::Endereço', {'relations': ('endereco', )}),
        ('Caixas::Caixas', {'fields': ('caixa_set',)}),
        ('Funcionários::Funcionários', {'relations': ('gerenteloja_set', 'vendedor_set', 'operadorprensa_set')}),
        ('Dados Bancários::Contas', {'relations': ('contas', )}),
        ('Estampagem::Prensas', {'relations': ('prensa_set',)}),
        ('Outras Informações::Configuração', {'fields': ('token', 'pode_enviar_pedido')}),
    )

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        from fabrica.models.producao import Kit, TipoProduto
        super(Loja, self).save(*args, **kwargs)
        for kit in Kit.objects.all():
            qs = KitLoja.objects.filter(kit=kit, loja=self)
            if not qs.exists():
                KitLoja.objects.create(kit=kit, loja=self, preco=kit.preco)
        for tipo_produto in TipoProduto.objects.all():
            qs = ConfiguracaoEstoqueLoja.objects.filter(tipo_produto=tipo_produto, loja=self)
            if not qs.exists():
                ConfiguracaoEstoqueLoja.objects.create(tipo_produto=tipo_produto, loja=self, quantidade_minima=0)

        if not self.token:
            self.token = '000'
            super(Loja, self).save(*args, **kwargs)


class FuncionarioLoja(Funcionario):
    loja = models.ForeignKey(Loja, verbose_name='Loja')

    class Meta:
        verbose_name = 'Funcionário'
        verbose_name_plural = 'Funcionários'
        abstract = True

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', ('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )


@role('cpf', scope='loja')
class GerenteLoja(FuncionarioLoja):

    class Meta:
        verbose_name = 'Gerente de Loja'
        verbose_name_plural = 'Gerentes de Loja'
        can_admin_by_organization = 'Administrador'
        menu = 'Sistêmico::Usuários::Gerentes de Loja', 'fa-th'
        icon = 'fa-users'
        usecase = 10


@role('cpf', scope='loja')
class Vendedor(FuncionarioLoja):

    class Meta:
        verbose_name = 'Vendedor'
        verbose_name_plural = 'Vendedores'
        can_admin_by_organization = 'Administrador'
        can_admin_by_unit = 'Gerente de Loja'
        menu = 'Sistêmico::Usuários::Vendedores', 'fa-th'
        icon = 'fa-users'
        usecase = 11


@role('cpf', scope='loja')
class OperadorPrensa(FuncionarioLoja):

    class Meta:
        verbose_name = 'Operador de Prensa'
        verbose_name_plural = 'Operadores de Prensa'
        can_admin_by_organization = 'Administrador'
        can_admin_by_unit = 'Gerente de Loja'
        menu = 'Sistêmico::Usuários::Operadores de Prensa', 'fa-th'
        icon = 'fa-users'
        usecase = 12


class Prensa(models.Model):
    loja = models.ForeignKey(Loja, verbose_name='Loja')
    descricao = models.CharField('Descrição', search=True)
    token = models.CharField(verbose_name='Token', null=True, blank=True, exclude=True)

    class Meta:
        verbose_name = 'Prensa'
        verbose_name_plural = 'Prensas'
        menu = 'Comunicação::Prensas', 'fa-external-link'
        can_admin_by_organization = 'Administrador'
        can_list_by_unit = 'Gerente de Loja'
        icon = 'fa-arrow-down'
        usecase = 13

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', 'descricao', 'token')}),
    )

    def save(self, *args, **kwargs):
        super(Prensa, self).save(*args, **kwargs)
        if not self.token:
            self.token = '000'
            super(Prensa, self).save(*args, **kwargs)


class KitLoja(models.Model):
    kit = models.ForeignKey('fabrica.Kit', verbose_name='Kit', filter=('categoria_veiculo', 'tipos_produtos__categoria_produto'), search=('descricao',))
    preco = models.MoneyField(verbose_name='Preço', default=Decimal(0))
    loja = models.ForeignKey(Loja, verbose_name='Loja', filter=True)

    class Meta:
        verbose_name = 'Kit de Produtos'
        verbose_name_plural = 'Kits de Produtos'
        menu = 'Movimentações::Kits de Produtos', 'fa-exchange'
        list_lookups = 'loja'
        can_list_by_unit = 'Gerente de Loja'
        can_list_by_organization = 'Gerente de Escritório'
        list_display = 'get_codigo', 'kit', 'preco'

    @action('Atualizar Preço', can_execute=('Gerente de Escritório', 'Gerente de Loja'), inline=True)
    def atualizar_preco(self, preco):
        self.save()

    def __str__(self):
        return '{} - R$ {}'.format(self.kit, self.preco)

    @meta('Código')
    def get_codigo(self):
        return self.kit.id


class ConfiguracaoEstoqueLojaManager(models.DefaultManager):

    # @subset('Aguardando Pedido', can_view='Gerente de Loja')
    def aguardando_reposicao(self):
        pks = []
        for configuracao in self.all():
            if configuracao.get_demanda_atual() > 0:
                pks.append(configuracao.pk)
        return self.filter(id__in=pks)

    # @action('Realizar Pedido de Reposição', can_execute='Gerente de Loja')
    def realizar_pedido(self):
        from fabrica.models.pedido import Pedido, ItemPedido
        pedido = Pedido()
        qs = self.all()
        if qs.exists():
            pedido.loja = qs[0].loja
            pedido.save()
            for configuracao in self.all():
                item = ItemPedido()
                quantidade = configuracao.get_demanda_atual()
                if quantidade:
                    item.tipo_produto = configuracao.tipo_produto
                    item.quantidade = quantidade
                    item.pedido = pedido
                    item.save()
        return pedido


class ConfiguracaoEstoqueLoja(models.Model):
    tipo_produto = models.ForeignKey('fabrica.TipoProduto', verbose_name='Tipo de Produto', filter=('categoria_veiculo',), search=('descricao', 'categoria_veiculo__descricao'), exclude=True)
    quantidade_minima = models.IntegerField(verbose_name='Qtd de Unidades', help_text='Quantidade mínima de unidades que a loja deseja ter em estoque.')
    quantidade_dias = models.IntegerField(verbose_name='Qtd de Dias', default=0, help_text='Quantidade de dias que será usado para calcular as últimas vendas. Por exemplo: Caso deseje que o estoque mínimo seja a quantidade de produtos vendidos no ultimo mês, informe o número 30.')
    loja = models.ForeignKey(Loja, verbose_name='Loja', exclude=True)

    fieldsets = (('Dados Gerais', {'fields': ('loja', 'tipo_produto')}),
                 ('Estoque Desejado', {'fields': ('quantidade_minima', 'quantidade_dias'), 'actions': ('alterar_quantidade_minima',)}),)

    class Meta:
        verbose_name = 'Configuração de Estoque'
        verbose_name_plural = 'Configuração de Estoque'
        list_lookups = 'loja',
        can_list_by_unit = 'Gerente de Loja'
        menu = 'Movimentações::Estoque::Configurar Estoque de Produtos', 'fa-exchange'
        list_per_page = 100
        list_display = 'tipo_produto', 'quantidade_minima', 'quantidade_dias'

    def __str__(self):
        return 'Configuração de estoque de {} na loja {}'.format(self.tipo_produto, self.loja)

    @action('Configurar', can_execute='Gerente de Loja', inline=True)
    def configurar(self, quantidade_minima, quantidade_dias):
        self.quantidade_minima = quantidade_minima
        self.quantidade_dias = quantidade_dias
        self.save()

    @meta('Estoque Desejado')
    def get_estoque_desejado(self):
        from fabrica.models.producao import Produto
        if not hasattr(self, 'estoque_desejado'):
            data_limite = datetime.date.today()
            data_limite = data_limite - datetime.timedelta(days=self.quantidade_dias)
            qtd_vendida = Produto.objects.all().vendidos().filter(tipo_produto=self.tipo_produto, solicitacaoimpressao__item_venda__venda__data__gte=data_limite).count()
            if self.quantidade_minima > qtd_vendida:
                self.estoque_desejado = self.quantidade_minima
            else:
                self.estoque_desejado = qtd_vendida
        return self.estoque_desejado

    @meta('Estoque Atual')
    def get_estoque_atual(self):
        from fabrica.models.producao import Produto
        if not hasattr(self, 'estoque_atual'):
            self.estoque_atual = Produto.objects.all().disponiveis_loja(self.loja).filter(tipo_produto=self.tipo_produto).count()
        return self.estoque_atual

    @meta('Qtd Solicitada')
    def get_qtd_solicitada(self):
        from fabrica.models.pedido import ItemPedido
        if not hasattr(self, 'qtd_solicitada'):
            self.qtd_solicitada = ItemPedido.objects.filter(pedido__loja=self.loja, pedido__data_solicitacao__isnull=False, tipo_produto=self.tipo_produto, pedido__data_cancelamento__isnull=True).exclude(pedido__data_recebimento_produtos__isnull=False).aggregate(Sum('quantidade')).get('quantidade__sum') or 0
        return self.qtd_solicitada

    @meta('Reposição Necessária')
    def get_demanda_atual(self):
        demanda_atual =  self.quantidade_minima - self.get_estoque_atual() - self.get_qtd_solicitada()
        return demanda_atual > 0 and demanda_atual or 0
