# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError
from djangoplus.db import models
from django.db.models.aggregates import Sum
from djangoplus.decorators import action, subset, meta
import datetime


class Banco(models.Model):
    numero = models.CharField(verbose_name='Número')
    descricao = models.CharField(verbose_name='Descrição', search=True)
    ativo = models.BooleanField('Ativo', default=True, filter=True, blank=True)

    fieldsets = (('Dados Gerais', {'fields': ('numero', 'descricao', 'ativo')}),)

    class Meta:
        verbose_name = 'Banco'
        verbose_name_plural = 'Bancos'
        menu = 'Financeiro::Bancos', 'fa-usd'

    def __str__(self):
        return '{} - {}'.format(self.numero, self.descricao)


class ContaBancaria(models.Model):
    banco = models.ForeignKey(Banco, verbose_name='Banco', filter=True)
    numero_conta = models.OneDigitValidationField('Número', search=True)
    numero_agencia = models.OneDigitValidationField('Agência', search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('banco', ('numero_conta', 'numero_agencia'))}),
    )

    class Meta:
        verbose_name = 'Conta'
        verbose_name_plural = 'Contas'

    def __str__(self):
        return '{} ({})'.format(self.numero_conta, self.banco.descricao)


class Bandeira(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)

    class Meta:
        verbose_name = 'Bandeira'
        verbose_name_plural = 'Bandeiras'
        verbose_female = True
        menu = 'Financeiro::Bandeiras', 'fa-usd'

    def __str__(self):
        return self.descricao


class CartaoCredito(models.Model):
    numero = models.CreditCardField(verbose_name='Número', search=True)
    bandeira = models.ForeignKey(Bandeira, verbose_name='Bandeira', filter=True)
    fieldsets = (
        ('Dados Gerais', {'fields': (('bandeira', 'numero',),)}),
    )

    class Meta:
        verbose_name = 'Cartão de Crédito'
        verbose_name_plural = 'Cartões de Crédito'

    def __str__(self):
        return self.numero


class ClasseImposto(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)

    class Meta:
        verbose_name = 'Classe de Imposto'
        verbose_name_plural = 'Classes de Impostos'
        verbose_female = True
        can_admin = 'Administrador Sistêmico'

    def __str__(self):
        return self.descricao


class NomenclaturaComumMercosul(models.Model):
    codigo = models.CharField(verbose_name='Código', search=True)
    descricao = models.CharField(verbose_name='Descrição', search=True, null=True, blank=True)

    class Meta:
        verbose_name = 'Nomenclatura Comum do Mercosul'
        verbose_name_plural = 'Nomenclaturas Comuns do Mercosul'
        verbose_female = True
        can_admin = 'Administrador Sistêmico'

    def __str__(self):
        return '{} - {}'.format(self.codigo, self.descricao)


class PlanoConta(models.Model):
    plano_conta = models.ForeignKey('fabrica.PlanoConta', verbose_name='Plano de Conta', null=True, blank=True, tree=True)
    cliente = models.ForeignKey('fabrica.Cliente', verbose_name='Cliente')
    numeracao = models.CharField(verbose_name='Numeração', exclude=True)
    descricao = models.CharField(verbose_name='Descrição')

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', 'plano_conta', 'numeracao', 'descricao')}),
    )

    class Meta:
        verbose_name = 'Plano de Conta'
        verbose_name_plural = 'Plano de Contas'
        menu = 'Financeiro::Planos de Conta', 'fa-usd'
        list_display = 'numeracao', 'descricao'
        can_admin = 'Gerente de Escritório'  # TODO can_admin_by_organization

    def __str__(self):
        return self.descricao

    def save(self, *args, **kwargs):
        if self.plano_conta_id:
            qs = PlanoConta.objects.filter(plano_conta=self.plano_conta_id).order_by('numeracao')
            numeracao = qs.exists() and int(qs[0].numeracao.split('.')[-1]) or 0
            numeracao += 1
            self.numeracao = '{}.{}'.format(self.plano_conta.numeracao, str(numeracao).zfill(2))
        else:
            if self.descricao == 'Receita':
                self.numeracao = '01'
            else:
                self.numeracao = '02'
        super(PlanoConta, self).save(*args, **kwargs)

    def can_add(self):
        return (self.plano_conta_id and not self.pk) or self.numeracao and len(self.numeracao.split('.')) < 3

    def can_edit(self):
        return len(self.numeracao.split('.')) > 1

    def can_delete(self):
        return len(self.numeracao.split('.')) > 2


class ContaReceberManager(models.DefaultManager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Recebidas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Recebidas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaReceber(models.Model):
    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja', null=True)
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    plano_conta = models.ForeignKey(PlanoConta, verbose_name='Plano de Conta', filter=True)
    pessoa = models.ForeignKey('fabrica.Pessoa', verbose_name='Devedor', null=False, blank=False, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    especie = models.ForeignKey('fabrica.EspeciePagamento', verbose_name='Espécie', null=True, blank=False, filter=True, exclude=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', ('descricao', 'plano_conta'), ('pessoa', 'valor'), ('data_solicitacao', 'data_prevista_pagamento'))}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago', 'especie'),)}),
        ('Outras Informações', {'fields': ('observacao',)})
    )

    class Meta:
        verbose_name = 'Conta a Receber'
        verbose_name_plural = 'Contas a Receber'
        menu = 'Financeiro::Contas a Receber::Outros Recebimentos', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'
        can_admin_by_organization = 'Gerente de Escritório'

    def choices(self):
        qs = PlanoConta.objects.all(self._user).filter(plano_conta__plano_conta__descricao='Receita')
        return dict(plano_conta=qs)

    def __str__(self):
        return self.descricao

    @action('Registrar Recebimento', condition='not data_pagamento', inline=True)
    def registrar_recebimento(self, data_pagamento, valor_pago, especie):
        self.data_pagamento = data_pagamento
        self.valor_pago = valor_pago
        self.especie = especie
        self.save()

    @action('Cancelar Recebimento', condition='data_pagamento')
    def cancelar_recebimento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()


class ContaPagarManager(models.DefaultManager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Pagas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Pagas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaPagar(models.Model):
    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja', null=True)
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    plano_conta = models.ForeignKey(PlanoConta, verbose_name='Tipo', filter=True)
    pessoa = models.ForeignKey('fabrica.Pessoa', verbose_name='Credor', null=False, blank=False, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    especie = models.ForeignKey('fabrica.EspeciePagamento', verbose_name='Espécie', null=True, blank=False, filter=True, exclude=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', ('descricao', 'plano_conta'), ('pessoa', 'valor'), ('data_solicitacao', 'data_prevista_pagamento'))}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago', 'especie'),)}),
        ('Outras Informações', {'fields': ('observacao',)})
    )

    class Meta:
        verbose_name = 'Conta a Pagar'
        verbose_name_plural = 'Contas a Pagar'
        menu = 'Financeiro::Contas a Pagar', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'
        can_admin_by_organization = 'Gerente de Escritório'

    def choices(self):
        qs = PlanoConta.objects.all(self._user).filter(plano_conta__plano_conta__descricao='Despesa')
        return dict(plano_conta=qs)

    def __str__(self):
        return self.descricao

    @action('Registrar Pagamento', condition='not data_pagamento', inline=True)
    def registrar_pagamento(self, data_pagamento, valor_pago, especie):
        self.data_pagamento = data_pagamento
        self.valor_pago = valor_pago
        self.especie = especie
        self.save()

    @action('Cancelar Pagamento', condition='data_pagamento')
    def cancelar_pagamento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()


class EspeciePagamento(models.Model):
    DINHEIRO = 1
    CHEQUE = 2
    CHEQUE_PRE = 3
    CARTAO_CREDITO = 4
    CARTAO_DEBITO = 5
    TRANSFERENCIA = 6

    descricao = models.CharField('Descrição')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',)}),
    )

    class Meta:
        verbose_name = 'Espécie de Pagamento'
        verbose_name_plural = 'Espécies de Pagamento'

    def __str__(self):
        return self.descricao


class Caixa(models.Model):
    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja', filter=True)
    descricao = models.CharField('Descrição', search=True)
    ativo = models.NullBooleanField('Ativo', default=True, exclude=True)
    vendedor = models.ForeignKey('fabrica.Vendedor', verbose_name='Operador no Momento', null=True, exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', 'descricao', ('ativo', 'vendedor'))}),
        ('Movimentações::Movimentações Financeiras', {'relations': ('movimentacao_set', ), 'actions' : ('iniciar_venda', 'receber_cheque', 'receber_cartao', 'receber_dinheiro', 'realizar_pagamento', 'realizar_transferencia')}),
        ('Vendas::Vendas do Caixa', {'relations': ('get_vendas',)}),
        ('Resumo::Gráficos', {'extra': ('get_total_por_especie',)}),
    )

    class Meta:
        verbose_name = 'Caixa'
        verbose_name_plural = 'Caixas'
        menu = 'Financeiro::Caixas', 'fa-usd'
        can_list_by_unit = 'Vendedor', 'Gerente de Loja'
        can_admin_by_organization = 'Administrador'
        icon = 'fa-desktop'
        list_shortcut = 'Vendedor', 'Gerente de Loja'

    @meta('Vendas')
    def get_vendas(self):
        return self.venda_set.all()

    @action('Fechar', condition='vendedor', can_execute=('Vendedor', 'Gerente de Loja'))
    def fechar(self):
        self.vendedor = None
        self.save()

    @action('Abrir', condition='not vendedor', input='fabrica.Movimentacao', can_execute='Vendedor', inline=True, redirect_to='/view/fabrica/caixa/{{ self.pk }}/')
    def abrir(self, valor):
        from fabrica.models import Vendedor
        qs = Vendedor.objects.filter(cpf=self._user.username)
        if qs.exists():
            self.vendedor = qs[0]
            self.save()
        else:
            raise ValidationError('Apenas vendedores podem abrir o caixa')
        Movimentacao.objects.create(caixa=self, especie_id=EspeciePagamento.DINHEIRO, descricao='Abertura do Caixa', data=datetime.datetime.now(), valor=valor)

    @action('Ativar', condition='not ativo', can_execute='Gerente de Loja', inline=True)
    def ativar(self):
        self.ativo = True
        self.save()

    @action('Inativar', condition='ativo', can_execute='Administrador', inline=True)
    def inativar(self):
        self.ativo = False
        self.save()

    @action('Iniciar Venda', category=None, condition='vendedor', can_execute='Vendedor', input='fabrica.venda', redirect_to='/view/fabrica/venda/{{ self.nova_venda.pk }}/')
    def iniciar_venda(self, comprador):
        from fabrica.models import Venda
        self.nova_venda = Venda()
        self.nova_venda.caixa = self
        self.nova_venda.vendedor = self.vendedor
        self.nova_venda.comprador = comprador
        self.nova_venda.save()

    @action('Realizar Pagamento', can_execute='Vendedor', condition='vendedor', category=None, input='RealizarPagamentoCaixaForm')
    def realizar_pagamento(self, conta, valor):
        especie = EspeciePagamento.objects.get(pk=EspeciePagamento.DINHEIRO)
        conta.registrar_pagamento(datetime.datetime.now(), valor, especie)
        Movimentacao.objects.create(caixa=self, especie_id=EspeciePagamento.DINHEIRO, descricao=conta.descricao, data=datetime.datetime.now(), valor=-valor)

    def realizar_pagamento_choices(self):
        return dict(conta=ContaPagar.objects.filter(data_pagamento__isnull=True))

    @action('Realizar Transferência', can_execute='Vendedor', condition='vendedor', category=None, input='fabrica.movimentacao')
    def realizar_transferencia(self, valor):
        pass

    def __str__(self):
        return self.descricao

    @meta('Total por Espécie de Pagamento', formatter='chart')
    def get_total_por_especie(self):
        return self.movimentacao_set.sum('valor', 'especie')


class Movimentacao(models.Model):
    caixa = models.ForeignKey(Caixa, verbose_name='Caixa')
    especie = models.ForeignKey(EspeciePagamento, verbose_name='Espécie', filter=True, null=True)
    descricao = models.CharField('Descrição', exclude=True, search=True)
    data = models.DateTimeField('Data', default=datetime.datetime.now, exclude=True)
    valor = models.DecimalField('Valor')

    venda = models.ForeignKey('fabrica.Venda', verbose_name='Venda', null=True, exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('caixa', 'especie'), ('data', 'valor'), ('descricao', 'venda'))}),
    )

    class Meta:
        verbose_name = 'Movimentação'
        verbose_name_plural = 'Movimentações'
        list_total = 'valor'
        can_list = 'Vendedor'

    def __str__(self):
        return self.descricao


class RecebimentoVenda(models.Model):

    venda = models.ForeignKey('fabrica.Venda', verbose_name='Venda')
    especie = models.ForeignKey(EspeciePagamento, verbose_name='Espécie', filter=True, null=True)
    data = models.DateField('Data', default=datetime.date.today, exclude=True)
    valor = models.DecimalField('Valor')

    consolidado = models.BooleanField('Consolidado', default=False, filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('venda', 'especie'), ('valor', 'data'), 'consolidado')}),
    )

    class Meta:
        verbose_name = 'Recebimento'
        verbose_name_plural = 'Recebimentos'
        can_list = 'Vendedor'
        can_delete = 'Vendedor'
        can_list_by_organization = 'Gerente de Escritório'
        list_lookups = 'venda__caixa__loja'

    def __str__(self):
        return 'Recebimento #{}'.format(self.pk)

    @action('Consolidar', can_execute='Gerente de Escritório', inline=True, condition='not consolidado')
    def consolidar(self):
        self.consolidado = True
        self.save()

    def can_delete(self):
        return not self.venda.pagamento_foi_finalizado()


class RecebimentoVendaDinheiro(RecebimentoVenda):

    class Meta:
        proxy = True
        can_list = 'Vendedor'
        verbose_name = 'Recebimento em Dinheiro'
        verbose_name_plural = 'Recebimentos em Dinheiro'

    def save(self, *args, **kwargs):
        self.consolidado = True
        self.especie = EspeciePagamento.objects.get(pk=EspeciePagamento.DINHEIRO)
        super(RecebimentoVendaDinheiro, self).save(*args, **kwargs)


class RecebimentoVendaCartaoCredito(RecebimentoVenda):

    cartao_credito = models.ForeignKey('fabrica.CartaoCredito', search=('numero',))

    fieldsets = RecebimentoVenda.fieldsets + (
        ('Cartão', {'fields': ('cartao_credito',)}),
    )

    class Meta:
        verbose_name = 'Recebimento por Cartão de Crédito'
        verbose_name_plural = 'Recebimentos por Cartão Crédito'
        menu = 'Financeiro::Contas a Receber::Cartão de Crédito', 'fa-usd'
        can_list_by_organization = 'Gerente de Escritório'
        list_lookups = 'venda__caixa__loja'

    def save(self, *args, **kwargs):
        self.especie = EspeciePagamento.objects.get(pk=EspeciePagamento.CARTAO_CREDITO)
        super(RecebimentoVendaCartaoCredito, self).save(*args, **kwargs)


class RecebimentoVendaCheque(RecebimentoVenda):

    conta = models.ForeignKey('fabrica.ContaBancaria', search=('numero',))
    numero_cheque = models.CreditCardField(verbose_name='Número', search=True)

    fieldsets = RecebimentoVenda.fieldsets + (('Cheque', {'fields': ('conta', 'numero_cheque')}),)

    class Meta:
        verbose_name = 'Recebimento por Cheque'
        verbose_name_plural = 'Recebimentos por Cheque'
        menu = 'Financeiro::Contas a Receber::Cheque', 'fa-usd'
        can_list_by_organization = 'Gerente de Escritório'
        list_lookups = 'venda__caixa__loja'

    def save(self, *args, **kwargs):
        self.especie = EspeciePagamento.objects.get(pk=EspeciePagamento.CHEQUE)
        super(RecebimentoVendaCheque, self).save(*args, **kwargs)


class RecebimentoVendaCartaoDebito(RecebimentoVenda):

    conta = models.ForeignKey('fabrica.ContaBancaria', search=('numero',))
    fieldsets = RecebimentoVenda.fieldsets + (('Cheque', {'fields': ('conta',)}),)

    class Meta:
        verbose_name = 'Recebimento por Cartão de Débito'
        verbose_name_plural = 'Recebimentos por Cartão de Débito'
        menu = 'Financeiro::Contas a Receber::Cartão de Débito', 'fa-usd'
        can_list_by_organization = 'Gerente de Escritório'
        list_lookups = 'venda__caixa__loja'

    def save(self, *args, **kwargs):
        self.especie = EspeciePagamento.objects.get(pk=EspeciePagamento.CARTAO_DEBITO)
        super(RecebimentoVendaCartaoDebito, self).save(*args, **kwargs)


class RecebimentoVendaTransferenciaBancaria(RecebimentoVenda):

    conta = models.ForeignKey('fabrica.ContaBancaria', verbose_name='Conta', filter=True, search=('numero',))
    numero_documento = models.IntegerField(verbose_name='Número do Documento')
    data_operacao = models.DateField(verbose_name='Data da Operação', null=True)
    fieldsets = RecebimentoVenda.fieldsets + (('Cheque', {'fields': ('conta', ('data_operacao', 'numero_documento'))}),)

    class Meta:
        verbose_name = 'Recebimento por Transferência Bancária'
        verbose_name_plural = 'Recebimentos por Transferência Bancária'
        menu = 'Financeiro::Contas a Receber::Transferência Bancária', 'fa-usd'
        can_list_by_organization = 'Gerente de Escritório'
        list_lookups = 'venda__caixa__loja'

    def save(self, *args, **kwargs):
        self.especie = EspeciePagamento.objects.get(pk=EspeciePagamento.TRANSFERENCIA)
        super(RecebimentoVendaTransferenciaBancaria, self).save(*args, **kwargs)




