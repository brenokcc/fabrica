# -*- coding: utf-8 -*-

from decimal import Decimal
from django.core.exceptions import ValidationError
from django.db.models.aggregates import Sum
from djangoplus.db import models
from djangoplus.decorators import action, subset, meta
from django.conf import settings
import datetime


class Fornecedor(models.Model):
    id = models.AutoField(verbose_name='Código', primary_key=True)
    cnpj = models.CnpjField(verbose_name='CNPJ', null=True, blank=True, search=True)
    razao_social = models.CharField(verbose_name='Razão Social', search=True, null=True)
    nome = models.CharField(verbose_name='Nome Fantasia', search=True)
    ie = models.CharField(verbose_name='Inscrição Estadual', null=True, blank=True)
    telefone_principal = models.PhoneField(verbose_name='Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name='Telefone Secundário', null=True, blank=True)
    responsavel = models.CharField(verbose_name='Contato', help_text='Nome do funcionário com que os contatos são estabelecidos', null=True, blank=True)
    email = models.EmailField(verbose_name='E-mail', blank=True, null=True)

    endereco = models.OneToOneField('enderecos.Endereco', verbose_name='Endereço', exclude=False, null=True, blank=True)

    class Meta:
        verbose_name = 'Fornecedor'
        verbose_name_plural = 'Fornecedores'
        menu = 'Movimentações::Produção::Fornecedores', 'fa-exchange'
        can_admin = 'Gerente de Produção'
        list_display = 'id', 'cnpj', 'nome'
        icon = 'fa-truck'
        usecase = 14

    fieldsets = (
        ('Dados Gerais', {'fields': (('razao_social', 'nome'), ('cnpj', 'ie'), )}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'responsavel', 'email')}),
        ('Endereço', {'relations': ('endereco', )}),
    )

    def __str__(self):
        return '{} - {} ({})'.format(self.id, self.nome, self.cnpj)


class MateriaPrimaManager(models.DefaultManager):

    @subset('Aguardando Reposição', can_view='Gerente de Produção', can_notify=True)
    def aguardando_reposicao(self):
        pks = []
        for materia_prima in self.all():
            if materia_prima.get_estoque() < materia_prima.estoque_critico:
                pks.append(materia_prima.pk)
        return self.filter(id__in=pks)

    def peliculas(self):
        return self.filter(pk__gt=1)


class MateriaPrima(models.Model):

    descricao = models.CharField(verbose_name='Descrição', search=True)
    estoque_critico = models.IntegerField(verbose_name='Estoque Crítico', help_text='Quantidade mínima que deve haver em estoque na fábrica.', default=0)

    fieldsets = (('Dados Gerais', {'fields': ('descricao', 'estoque_critico')}),)

    class Meta:
        verbose_name = 'Matéria-Prima'
        verbose_name_plural = 'Matérias-Primas'
        list_display = 'descricao', 'get_unidade_medida', 'estoque_critico', 'get_estoque'
        verbose_female = True
        can_admin = 'Gerente de Produção'
        can_list = 'Gerente de Produção'
        menu = 'Movimentações::Estoque::Visualizar Estoque de Matérias-Primas', 'fa-exchange'
        icon = 'fa-crop'
        list_shortcut = True
        usecase = 15

    def __str__(self):
        return self.descricao

    @meta('Unidade de Medida')
    def get_unidade_medida(self):
        return self.pk == 1 and 'g' or 'mm²'

    @meta('Estoque')
    def get_estoque(self, excluir_item_entrada=None):
        if self.pk == 1:
            saida = Producao.objects.all().aggregate(Sum('aluminio')).get('aluminio__sum') or 0
        else:
            saida = Producao.objects.filter(tipo_pelicula=self).aggregate(Sum('pelicula')).get('pelicula__sum') or 0
        qs_entrada = ItemEntradaMateriaPrima.objects.filter(materia_prima=self)
        if excluir_item_entrada:
            qs_entrada = qs_entrada.exclude(pk=excluir_item_entrada.pk)
        entrada = qs_entrada.aggregate(Sum('quantidade')).get('quantidade__sum') or 0
        return entrada - saida

    @action('Alterar Estoque Crítico', can_execute='Gerente de Produção', inline=True)
    def alterar_estoque_critico(self, estoque_critico):
        self.estoque_critico = estoque_critico
        self.save()

    def can_edit(self):
        return self.pk != 1

    def can_delete(self):
        return self.can_edit()


class EntradaMateriaPrima(models.Model):
    data = models.DateField(verbose_name='Data', default=datetime.date.today, exclude=True)
    fornecedor = models.ForeignKey(Fornecedor, verbose_name='Fornecedor', lazy=True)
    nota_fiscal = models.CharField(verbose_name='Número da Nota Fiscal')

    fieldsets = (('Dados Gerais', {'fields': (('data', 'nota_fiscal'), 'fornecedor')}),
        ('Matéria-Prima', {'relations': ('itementradamateriaprima_set',)}),
    )

    class Meta:
        verbose_name = 'Entrada de Matéria-Prima'
        verbose_name_plural = 'Entradas Matéria-Prima'
        add_message = 'Por favor, adicione as matérias-primas adquiridas.'
        can_add = 'Gerente de Produção'
        can_list = 'Gerente de Produção', 'Administrador Sistêmico'
        can_edit = 'Administrador Sistêmico'
        can_delete = 'Administrador Sistêmico'
        menu = 'Movimentações::Produção::Entradas Matéria-Prima', 'fa-exchange'
        verbose_female = True
        icon = 'fa-arrow-circle-down'
        add_shortcut = True
        usecase = 16
        class_diagram = 'itementradamateriaprima', 'materiaprima', 'categoriaveiculo', 'fornecedor'

    def __str__(self):
        return 'Entrada de Matéria-Prima #{}'.format(self.pk)


class ItemEntradaMateriaPrima(models.Model):
    entrada_materia_prima = models.ForeignKey(EntradaMateriaPrima, verbose_name='Entrada', composition=True)
    materia_prima = models.ForeignKey(MateriaPrima, verbose_name='Matéria-Prima')
    quantidade = models.IntegerField(verbose_name='Quantidade', suffix='get_unidade_medida')

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'
        can_admin = 'Administrador Sistêmico'
        can_add = 'Gerente de Produção'
        usecase = 17

    def __str__(self):
        return 'Item #{} da Entrada #{}'.format(self.pk, self.entrada_materia_prima.pk)

    @meta('Unidade de Medida')
    def get_unidade_medida(self):
        return self.materia_prima.pk == 1 and 'g' or 'mm²'

    def clean(self):
        if self.quantidade <= 0:
            raise ValidationError('A quantidade deve ser um número positivo.')
        estoque = self.materia_prima.get_estoque(self)
        if estoque + self.quantidade < 0:
            raise ValidationError('Quantidade inválida, pois faria com que o estoque do item ficasse negativo.')


class ProducaoManager(models.DefaultManager):

    @subset('Do Dia')
    def do_dia(self):
        return self.filter(data=datetime.date.today())

    @subset('Não-Concluídas', can_view=('Gerente de Produção', 'Operador de Produção'), can_notify=True)
    def nao_finalizadas(self):
        return self.filter(data_conclusao__isnull=True)


class Producao(models.Model):
    id = models.AutoField(verbose_name='Número', primary_key=True)
    tipo_produto = models.ForeignKey('fabrica.TipoProduto', verbose_name='Tipo de Produto', filter=True)

    data = models.DateField(verbose_name='Data da Solicitação', default=datetime.date.today, exclude=True)

    tipo_pelicula = models.ForeignKey(MateriaPrima, verbose_name='Tipo de Película', filter=True)
    pelicula = models.IntegerField(verbose_name='Película Utilizada', help_text='Consumo em mm²')
    aluminio = models.IntegerField(verbose_name='Alumínio Utilizado', help_text='Consumo em g')

    quantidade_solicitada = models.IntegerField(verbose_name='Quantidade Prevista', null=True, help_text='Quantidade de produtos a serem produzidos')
    quantidade_produzida = models.IntegerField(verbose_name='Quantidade Produzida', null=True, help_text='Quantidade de produtos efetivamente produzidos', exclude=True)
    data_conclusao = models.DateField(verbose_name='Data da Conclusão', null=True, exclude=True)

    sequencial_inicial = models.IntegerField(verbose_name='Sequencial Inicial', null=True, blank=True, exclude=True, help_text='Número a partir do qual o código de identificador do produto (XDDmAANNNNNN) será gerado.')

    fieldsets = (('Dados Gerais', {'fields': (('tipo_produto', 'tipo_pelicula'), ('data', 'data_conclusao') ,('quantidade_solicitada', 'quantidade_produzida'))}),
        ('Matéria-Prima::Matéria-Prima', {'fields': (('pelicula', 'aluminio'),('get_desperdicio_pelicula', 'get_desperdicio_aluminio')), 'can_view' : ('Gerente de Produção',), 'actions' : ('informar_materia_prima_utilizada',)}),
        ('Produtos::Produtos', {'fields':('get_faixa_producao',), 'relations': ('get_itens_produzidos', 'get_itens_nao_produzidos')}),
    )

    class Meta:
        verbose_name = 'Produção'
        verbose_name_plural = 'Produções'
        menu = 'Movimentações::Produção::Produções', 'fa-exchange'
        can_admin = 'Gerente de Produção'
        can_list = 'Operador de Produção'
        list_display = 'id', 'tipo_produto', 'data', 'data_conclusao', 'quantidade_solicitada', 'quantidade_produzida'
        icon = 'fa-crop'
        add_message = 'Produção cadastrada com sucesso'
        verbose_female = True
        add_shortcut = True
        list_shortcut = True

    def choices(self):
        return dict(tipo_pelicula=MateriaPrima.objects.all().peliculas())

    @meta('Película Desperdiçada')
    def get_desperdicio_pelicula(self):
        sobra = self.pelicula - self.quantidade_solicitada*self.tipo_produto.consumo_pelicula
        unidades_produzidas = self.get_itens_produzidos().count()
        unidades_nao_produzidas = self.get_itens_nao_produzidos().count()
        return sobra + unidades_produzidas*self.tipo_produto.desperdicio_pelicula + unidades_nao_produzidas*self.tipo_produto.consumo_pelicula

    @meta('Alumínio Desperdiçado')
    def get_desperdicio_aluminio(self):
        sobra = self.aluminio - self.quantidade_solicitada*self.tipo_produto.consumo_aluminio
        unidades_produzidas = self.get_itens_produzidos().count()
        unidades_nao_produzidas = self.get_itens_nao_produzidos().count()
        return unidades_produzidas*self.tipo_produto.desperdicio_aluminio + unidades_nao_produzidas*self.tipo_produto.consumo_aluminio

    @meta('Itens Produzidos')
    def get_itens_produzidos(self):
        return Produto.objects.filter(producao=self, produzido=True)

    @meta('Itens Não-produzidos')
    def get_itens_nao_produzidos(self):
        return Produto.objects.filter(producao=self, produzido=False)

    def __str__(self):
        return 'Produção #{}'.format(self.pk)

    def clean(self):
        pelicula = self.tipo_pelicula.get_estoque()
        aluminio = MateriaPrima.objects.get(pk=1).get_estoque()
        if self.pelicula > pelicula:
            raise ValidationError('Estoque insuficiente, pois o estoque atual de {} é de apenas {} mm²'.format(self.tipo_pelicula, pelicula))
        if self.aluminio > aluminio:
            raise ValidationError('Estoque insuficiente, pois o  estoque atual de alumínio é de apenas {} g'.format(self.aluminio))

    def save(self, *args, **kwargs):

        if not self.pk:
            super(Producao, self).save(*args, **kwargs)
            if Produto.objects.exists():
                self.sequencial_inicial = int(Produto.objects.latest('id').codigo[-6:])+1
            else:
                self.sequencial_inicial = 1

            Produto.objects.filter(producao=self).delete()

            sequenciais = []
            for sequencial in range(self.sequencial_inicial, self.sequencial_inicial + self.quantidade_solicitada):
                sequencial = '0000000000{}'.format(sequencial)
                sequencial = '{}{}'.format(datetime.date.today().strftime("%Y%m%d"), sequencial[-6:])
                sequenciais.append(sequencial)

            if Produto.objects.filter(codigo__in=sequenciais).exists():
                raise ValidationError('O sequencial informado gera conflito com os produtos já fabricados')

            for sequencial in sequenciais:
                Produto.objects.create(producao=self, tipo_produto=self.tipo_produto, codigo=sequencial)
        else:
            super(Producao, self).save(*args, **kwargs)

    @meta('Faixa de Produção')
    def get_faixa_producao(self):
        qs = Produto.objects.filter(producao=self).order_by('id')
        if qs.exists():
            return '{} até {}'.format(qs[0].codigo, qs[qs.count()-1].codigo)
        else:
            return '-'

    @meta('Primeiro Produto')
    def get_primeiro_produto(self):
        qs = Produto.objects.filter(producao=self).order_by('id')
        return qs.exists() and qs[0] or None

    @meta('Último Produto')
    def get_ultimo_produto(self):
        qs = Produto.objects.filter(producao=self).order_by('-id')
        return qs.exists() and qs[0] or None


class CategoriaVeiculo(models.Model):
    nome = models.CharField(verbose_name='Nome', default='')
    descricao = models.TextField(verbose_name='Descrição')
    exemplo = models.ImageField(verbose_name='Exemplo', upload_to='categorias', null=True, blank=True)

    class Meta:
        verbose_name = 'Categoria de Veículo'
        verbose_name_plural = 'Categorias de Veículo'
        menu = 'Movimentações::Categorias de Veículo', 'fa-th'
        can_list = 'Administrador Sistêmico'
        ordering = ('id',)

    def __str__(self):
        return self.nome


class TipoProdutoManager(models.DefaultManager):

    @subset('Aguardando Produção', can_view='Gerente de Produção', can_notify=True)
    def aguardando_producao(self):
        ids = []
        for tipo_produto in self.all():
            if tipo_produto.get_estoque_atual() + tipo_produto.get_em_producao() < tipo_produto.get_demanda_atual():
                ids.append(tipo_produto.id)
        return self.filter(id__in=ids)


class TipoProduto(models.Model):
    descricao = models.CharField(verbose_name='Descrição')

    consumo_pelicula = models.IntegerField(verbose_name='Película', default=0, help_text='Consumo em mm²')
    consumo_aluminio = models.IntegerField(verbose_name='Alumínio', default=0, help_text='Consumo em g')

    desperdicio_pelicula = models.IntegerField(verbose_name='Película', default=0, help_text='Desperdício em mm²')
    desperdicio_aluminio = models.IntegerField(verbose_name='Alumínio', default=0, help_text='Desperdício em g')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',)}),
        ('Consumo de Matéria-Prima por Unidade',{'fields': (('consumo_pelicula', 'consumo_aluminio'),), 'actions': ('informar_consumo',)}),
        ('Despedício de Matéria-Prima por Unidade', {'fields': (('desperdicio_pelicula', 'desperdicio_aluminio'),)}),
    )

    class Meta:
        verbose_name = 'Tipo de Produto'
        verbose_name_plural = 'Tipos de Produtos'
        can_list = 'Administrador Sistêmico', 'Gerente de Produção'
        list_display = 'descricao', 'get_estoque_atual', 'get_em_producao', 'get_demanda_atual', 'get_reposicao_necessaria'
        menu = 'Movimentações::Tipos de Produtos', 'fa-exchange'
        list_shortcut = True
        class_diagram = True

    def __str__(self):
        return self.descricao

    @action('Imprimir Guia de Produção', can_execute='Gerente de Produção', style='pdf', category=False)
    def imprimir_ordem_producao(self):
        tipos_produtos = TipoProduto.objects.filter(pk=self.pk)
        hoje = datetime.date.today()
        return locals()

    @meta('Estoque Atual')
    def get_estoque_atual(self):
        if not hasattr(self, 'estoque_atual'):
            self.estoque_atual = Produto.objects.filter(tipo_produto=self, loja__isnull=True, produzido=True).count()
        return self.estoque_atual

    @meta('Em Produção')
    def get_em_producao(self):
        if not hasattr(self, 'em_producao'):
            self.em_producao = Producao.objects.filter(tipo_produto=self, quantidade_produzida__isnull=True).aggregate(Sum('quantidade_solicitada')).get('quantidade_solicitada__sum') or 0
        return self.em_producao

    @meta('Demanda Atual')
    def get_demanda_atual(self):
        from fabrica.models.pedido import ItemPedido
        if not hasattr(self, 'demanda_atual'):
            self.demanda_atual = ItemPedido.objects.filter(tipo_produto=self, pedido__data_aprovacao__isnull=False, pedido__data_cancelamento__isnull=True).exclude(pedido__data_recebimento_produtos__isnull=False).aggregate(Sum('quantidade')).get('quantidade__sum') or 0
        return self.demanda_atual > 0 and self.demanda_atual or 0

    @meta('Reposição Necessária')
    def get_reposicao_necessaria(self):
        reposicao_necessaria = 0
        reposicao_necessaria = self.get_demanda_atual() - self.get_em_producao() - self.get_estoque_atual()
        return reposicao_necessaria > 0 and reposicao_necessaria or 0


class ProdutoManager(models.DefaultManager):

    @subset('Produzidos', can_view=('Gerente de Produção',))
    def produzidos(self):
        return self.filter(produzido=True)

    @subset('Em produção', can_view=('Gerente de Produção',))
    def aguardando_producao(self):
        return self.filter(produzido=False)

    @subset('Disponíveis na Fábrica', can_view='Gerente de Produção')
    def disponiveis_fabrica(self):
        return self.produzidos().filter(loja__isnull=True).filter(pedido__data_envio_produtos__isnull=True)

    @subset('Disponíveis na Loja', can_view=('Gerente de Loja', 'Vendedor'))
    def disponiveis_loja(self, loja=None):
        if loja:
            qs = self.produzidos().filter(loja=loja)
        else:
            qs = self.produzidos().filter(loja__isnull=False)
        qs = qs.exclude(pedido__data_recebimento_produtos__isnull=True)
        qs = qs.exclude(solicitacaoimpressao__isnull=False)
        return qs

    @subset('Em Trânsito', can_view=('Gerente de Produção', 'Gerente de Escritório', 'Gerente de Loja'))
    def em_transito(self):
        return self.produzidos().filter(pedido__data_envio_produtos__isnull=False, pedido__data_recebimento_produtos__isnull=True)

    @subset('Vendidos', can_view=('Gerente de Loja', 'Vendedor'))
    def vendidos(self):
        return self.produzidos().filter(solicitacaoimpressao__isnull=False).distinct()


class Produto(models.Model):
    producao = models.ForeignKey(Producao, verbose_name='Produção')
    tipo_produto = models.ForeignKey(TipoProduto, verbose_name='Tipo', filter=True)
    codigo = models.CharField(verbose_name='Identificação', search=True)
    produzido = models.BooleanField(verbose_name='Produzido?', default=False)
    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja', null=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('producao', 'produzido'), ('tipo_produto', 'codigo'), 'qrcode')}),
        ('Rastreabilidade', {'fields': (('loja', 'get_venda'), ('get_veiculo', 'get_proprietario'),), 'relations' : ('get_transferencias',)}),
    )

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        menu = 'Movimentações::Estoque::Listar Produtos', 'fa-exchange'
        list_lookups = 'loja'
        list_display = 'tipo_produto', 'get_tipo_pelicula', 'codigo', 'loja', 'qrcode'
        can_list = 'Gerente de Produção'
        can_list_by_organization = 'Gerente de Escritório'
        can_list_by_unit = 'Gerente de Loja', 'Vendedor'
        icon = 'fa-puzzle-piece'
        list_shortcut = True

    def __str__(self):
        return self.codigo

    @meta('QrCode', formatter='qrcode')
    def qrcode(self):
        return self.codigo

    @meta('Transferências')
    def get_transferencias(self):
        from fabrica.models.pedido import TransferenciaProduto
        return TransferenciaProduto.objects.filter(produtos=self)

    @meta('Proprietário')
    def get_proprietario(self):
        if self.solicitacaoimpressao_set.exists():
            return self.solicitacaoimpressao_set.all()[0].item_venda.proprietario

    @meta('Venda')
    def get_venda(self):
        if self.solicitacaoimpressao_set.exists():
            return self.solicitacaoimpressao_set.all()[0].item_venda.venda

    @meta('Veículo')
    def get_veiculo(self):
        if self.solicitacaoimpressao_set.exists():
            return self.solicitacaoimpressao_set.all()[0].item_venda.veiculo

    @meta('Tipo de Película')
    def get_tipo_pelicula(self):
        return self.producao.tipo_pelicula

    def save(self, *args, **kwargs):
        super(Produto, self).save(*args, **kwargs)


class Kit(models.Model):
    id = models.AutoField(verbose_name='Código', primary_key=True)
    descricao = models.CharField(verbose_name='Descrição', search=True)
    categoria_veiculo = models.ForeignKey(CategoriaVeiculo, verbose_name='Categoria', null=True, filter=True)
    preco = models.MoneyField(verbose_name='Preço', default=Decimal(0))
    tipos_produtos = models.ManyToManyField(TipoProduto, verbose_name='Tipos de Produtos')

    fieldsets = (('Dados Gerais', {'fields': ('descricao', ('categoria_veiculo', 'preco'), 'tipos_produtos'),}),)

    class Meta:
        verbose_name = 'Kit de Produtos'
        verbose_name_plural = 'Kits de Produtos'
        can_admin = 'Administrador Sistêmico'
        menu = 'Movimentações::Kits de Produtos', 'fa-exchange'
        list_display = 'id', 'descricao', 'categoria_veiculo', 'preco', 'get_tipos_produtos'
        list_shortcut = True
        class_diagram = True

    def __str__(self):
        return '{} -  {}'.format(self.descricao, self.categoria_veiculo)

    @action('Atualizar Preço Padrão', can_execute=('Administrador', 'Gerente de Escritório'), inline=True)
    def atualizar_preco(self, preco):
        self.save()

    @meta('Produtos')
    def get_tipos_produtos(self):
        l = []
        for tipo_produto in self.tipos_produtos.all():
            l.append(str(tipo_produto))
        return ', '.join(l)