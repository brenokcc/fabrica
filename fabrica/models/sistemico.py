# -*- coding: utf-8 -*-

from djangoplus.admin.models import Group
from djangoplus.db import models
from djangoplus.decorators import role


class PerfilUsuario(Group):
    class Meta:
        proxy = True
        menu = 'Sistêmico::Perfis de Usuário', 'fa-th'
        verbose_name = 'Perfil de Usuário'
        verbose_name_plural = 'Perfis de Usuários'


class Funcionario(models.Model):
    cpf = models.CpfField(verbose_name='CPF', search=True)
    nome = models.CharField('Nome', search=True)
    email = models.CharField('E-mail', blank=True, default='')
    telefone_principal = models.PhoneField(verbose_name='Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField(verbose_name='Telefone Secundário', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('cpf', 'nome'), 'email')}),
        ('Dados para Contato', {'fields': (('telefone_principal', 'telefone_secundario'),)}),
    )

    class Meta:
        verbose_name = 'Funcionário'
        verbose_name_plural = 'Funcionários'
        abstract = True

    def __str__(self):
        return self.nome


@role('cpf')
class AdministradorSistemico(Funcionario):
    class Meta:
        verbose_name = 'Administrador Sistêmico'
        verbose_name_plural = 'Administradores Sistêmicos'
        menu = 'Sistêmico::Usuários::Administradores Sistêmicos', 'fa-th'


@role('cpf')
class GerenteProducao(Funcionario):
    class Meta:
        verbose_name = 'Gerente de Produção'
        verbose_name_plural = 'Gerentes de Produção'
        can_admin = 'Administrador Sistêmico'
        menu = 'Sistêmico::Usuários::Gerentes de Produção', 'fa-th'
        usecase = 2


@role('cpf')
class GerenteEscritorioFabrica(Funcionario):
    class Meta:
        verbose_name = 'Gerente de Escritório da Fábrica'
        verbose_name_plural = 'Gerentes de Escritório da Fábrica'
        can_admin = 'Administrador Sistêmico'
        menu = 'Sistêmico::Usuários::Gerentes de Escritório da Fábrica', 'fa-th'
        usecase = 3


@role('cpf')
class OperadorProducao(Funcionario):
    class Meta:
        verbose_name = 'Operador de Produção'
        verbose_name_plural = 'Operadores de Produção'
        can_admin = 'Administrador Sistêmico'
        menu = 'Sistêmico::Usuários::Operadores de Produção', 'fa-th'
        usecase = 4


@role('cpf')
class FuncionarioFabrica(Funcionario):
    class Meta:
        verbose_name = 'Funcionário da Fábrica'
        verbose_name_plural = 'Funcionários da Fábrica'
        can_admin = 'Administrador Sistêmico'
        menu = 'Sistêmico::Usuários::Funcionários da Fábrica', 'fa-th'
        usecase = 5

