# -*- coding: utf-8 -*-

from decimal import Decimal
from django.core.exceptions import ValidationError
from django.db.models.aggregates import Sum
from djangoplus.db import models
from djangoplus.decorators import action, subset, meta
import datetime
from enderecos.models import Estado, Municipio
from djangoplus.utils.dateutils import add_months, add_days


class Pessoa(models.Model):
    nome = models.CharField('Nome', null=False, blank=False, search=True)
    documento = models.CharField('Documento', null=False, exclude=True, search=True, display=None)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoa'

    def __str__(self):
        return self.nome


class PessoaFisica(Pessoa):

    cpf = models.CpfField(verbose_name='CPF', search=True)

    rg = models.CharField(verbose_name='RG')
    cnh = models.CharField(verbose_name='CNH', blank=True, default='')

    arquivo = models.ImageField(verbose_name='Documento Digitalizado', null=True, blank=True)
    observacao = models.TextField(verbose_name='Observação', blank=True, default='')

    endereco = models.CharField(verbose_name='Endereço')

    telefone = models.PhoneField('Telefone', blank=True, default='')
    email = models.CharField(verbose_name='E-mail', blank=True, default='')

    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja')

    fieldsets = (('Dados Gerais', {'fields': ('loja', ('nome', 'cpf'), ('rg', 'cnh'), 'endereco', ('telefone', 'email'))}),
        ('Documentação', {'fields': ('arquivo', 'observacao')}),
        ('Dados Bancários', {'fields': ('contas', 'cartoes')}),
    )

    cartoes = models.OneToManyField('fabrica.CartaoCredito', verbose_name='Cartões', exclude=True, blank=True)
    contas = models.OneToManyField('fabrica.ContaBancaria', verbose_name='Contas Bancárias', exclude=True, blank=True)

    class Meta:
        verbose_name = 'Pessoa Física'
        verbose_name_plural = 'Pessoas Físicas'
        menu = 'Movimentações::Pessoas Físicas', 'fa-exchange'
        can_admin_by_unit = 'Vendedor', 'Gerente de Loja'
        can_admin_by_organization = 'Gerente de Escritório'
        icon = 'fa-user'
        list_lookups = 'loja'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        self.documento = self.cpf
        super(PessoaFisica, self).save(*args, **kwargs)


class PessoaJuridica(Pessoa):

    cnpj = models.CnpjField(verbose_name='CNPJ', search=True)

    endereco = models.CharField(verbose_name='Endereço')

    telefone = models.PhoneField('Telefone', blank=True, default='')
    email = models.CharField(verbose_name='E-mail', blank=True, default='')

    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja')

    fieldsets = (
        ('Dados Gerais', {'fields': ('loja', ('nome', 'cnpj'), 'endereco', ('telefone', 'email'))}),
    )

    cartoes = models.OneToManyField('fabrica.CartaoCredito', verbose_name='Cartões', exclude=True, blank=True)
    contas = models.OneToManyField('fabrica.ContaBancaria', verbose_name='Contas Bancárias', exclude=True, blank=True)

    class Meta:
        verbose_name = 'Pessoa Jurídica'
        verbose_name_plural = 'Pessoas Jurídicas'
        menu = 'Movimentações::Pessoas Jurídicas', 'fa-exchange'
        can_admin_by_unit = 'Vendedor', 'Gerente de Loja'
        can_admin_by_organization = 'Gerente de Escritório'
        icon = 'fa-user'
        list_lookups = 'loja'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        self.documento = self.cnpj
        super(PessoaJuridica, self).save(*args, **kwargs)


class VeiculoManager(models.DefaultManager):

    @subset('Cadastrados Hoje')
    def cadastros_hoje(self):
        return self.filter(data_cadastro=datetime.date.today())


class Veiculo(models.Model):

    crlv = models.CharField(verbose_name='CRLV')
    ano_exercicio = models.IntegerField(verbose_name='Ano de Exercício', choices=[[2015, 2015], [2016, 2016], [2017, 2017]])
    placa = models.CarPlateField(verbose_name='Placa', search=True)
    uf = models.ForeignKey(Estado, verbose_name='UF', null=True)
    categoria = models.ForeignKey('fabrica.CategoriaVeiculo')
    novo = models.BooleanField('Zero KM?', default=True)

    renavan = models.CharField(verbose_name='Renavam')
    chassi = models.CharField(verbose_name='Chassi')
    municipio = models.ForeignKey(Municipio, verbose_name='Município', lazy=True, null=True)
    arquivo = models.ImageField(verbose_name='Documento Digitalizado', null=True, blank=True)
    
    observacao = models.TextField(verbose_name='Observação', blank=True, default='')
    data_cadastro = models.DateField(verbose_name='Data do Cadastro', null=True, exclude=True, default=datetime.date.today, filter=True)

    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja')

    fieldsets = (('Dados Gerais', {'fields': ('loja', ('placa', 'uf', 'crlv', 'ano_exercicio'), 'novo')}),
        ('Documentação', {'fields': (('renavan', 'chassi', 'municipio', 'categoria'), 'arquivo', 'observacao')}),
    )
    
    class Meta:
        verbose_name = 'Veículo'
        verbose_name_plural = 'Veículos'
        menu = 'Veículos'
        can_admin_by_unit = 'Vendedor', 'Gerente de Loja'
        icon = 'fa-car'
        list_lookups = 'loja'
        
    def __str__(self):
        return self.placa


class VendaManager(models.DefaultManager):

    @subset('Aguardando Impressão', can_view=('Vendedor', 'Gerente de Loja'))
    def andamento(self):
        return self.filter(data_impressao=None)

    @subset('Aguardando Entrega', can_view=('Vendedor', 'Gerente de Loja'), can_notify=True)
    def aguarando_entrega(self):
        return self.filter(data_entrega=None)

    @subset('Finalizadas', can_view=('Vendedor', 'Gerente de Loja'))
    def finalizadas(self):
        return self.exclude(data_entrega=None)


class Venda(models.Model):
    comprador = models.ForeignKey(PessoaFisica, verbose_name='Comprador', lazy=True, search=('nome',))
    data = models.DateTimeField(verbose_name='Data', default=datetime.datetime.now, exclude=True, filter=True)
    data_impressao = models.DateTimeField(verbose_name='Data da Estampagem', exclude=True, null=True)
    data_entrega = models.DateTimeField(verbose_name='Data da Entrega', exclude=True, null=True)
    caixa = models.ForeignKey('fabrica.Caixa', verbose_name='Caixa', null=True, filter=('loja', 'id'))
    vendedor = models.ForeignKey('fabrica.Vendedor', verbose_name='Vendedor', null=True, filter=True, exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('caixa', 'data_impressao', 'data_entrega'), ('vendedor', 'comprador'))}),
        ('Itens da Venda::Itens', {'inlines': ('itemvenda_set',)}),
        ('Recebimento::Valores', {'fields': (('get_valor_total', 'get_valor_recebido', 'get_troco'),)}),
        ('Recebimento::Registros', {'relations': ('recebimentovenda_set',), 'actions': ('registrar_recebimento_dinheiro', 'registrar_recebimento_cartao_credito', 'registrar_recebimento_cheque', 'registrar_recebimento_cartacao_debito', 'registrar_recebimento_transferencia_bancaria', 'finalizar_pagamento')}),
    )

    class Meta:
        verbose_name = 'Venda'
        verbose_name_plural = 'Vendas'
        menu = 'Movimentações::Vendas', 'fa-exchange'
        add_message = 'Venda iniciada com sucesso'
        can_admin_by_unit = 'Gerente de Loja'
        can_list_by_role = 'Vendedor'
        list_display = 'id', 'comprador', 'data', 'get_status'
        verbose_female = True
        usecase = 30
        class_diagram = 'pessoafisica', 'itemvenda', 'kitloja', 'veiculo'
        list_lookups = 'caixa__loja'

    def __str__(self):
        return 'Venda #{}'.format(self.id)

    @meta('Status', formatter='status_venda')
    def get_status(self):
        if not self.data_impressao and not self.data_entrega:
            return 0, 'Em andamento'
        if not self.data_impressao:
            return 1, 'Aguardando Impressão'
        elif not self.data_entrega:
            return 2, 'Pronto para Entrega'
        else:
            return 3, 'Finalizada'

    @meta('Valor Total')
    def get_valor_total(self):
        total = 0
        for item in self.itemvenda_set.all():
            total += item.kit_loja.preco - item.desconto
        return total

    @meta('Valor Recebido')
    def get_valor_recebido(self):
        return self.recebimentovenda_set.all().aggregate(Sum('valor')).get('valor__sum') or Decimal(0)

    @meta('Troco')
    def get_troco(self):
        valor_total = self.get_valor_total()
        valor_recebido = self.get_valor_recebido()
        troco = valor_recebido - valor_total
        return troco > 0 and troco or Decimal(0)

    @action('Registrar Entrega', condition='pode_registrar_entrega', inline=True, category=None)
    def registrar_entrega(self):
        self.data_entrega = datetime.datetime.now()
        self.save()

    def pode_registrar_entrega(self):
        return self.data_impressao and not self.data_entrega or False

    def pode_registrar_pagamento(self):
        return self.itemvenda_set.exists() and not self.pagamento_foi_finalizado() and not self.pode_finalizar_pagamento()

    def pode_ser_priorizada(self):
        return SolicitacaoImpressao.objects.filter(item_venda__venda=self, data_impressao__isnull=True).exists()

    @action('Priorizar Impressão', condition='pode_ser_priorizada', category=None, icon='fa-arrow-up')
    def priorizar(self):
        for solicitacao in SolicitacaoImpressao.objects.filter(item_venda__venda=self, data_impressao__isnull=True).order_by('-posicao'):
            solicitacao.priorizar()

    @action('Dinheiro', input='fabrica.RecebimentoVenda', condition='pode_registrar_pagamento', category='Registrar Recebimento', icon='fa-money')
    def registrar_recebimento_dinheiro(self, valor):
        from fabrica.models.financeiro import RecebimentoVendaDinheiro, Movimentacao, EspeciePagamento
        data = datetime.datetime.now()
        RecebimentoVendaDinheiro(venda=self, data=data, valor=valor).save()
        #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.DINHEIRO, descricao='Venda', data=data, valor=valor)

    @action('Cartão de Crédito', input='ReceberCartaoCreditoForm', condition='pode_registrar_pagamento', category='Registrar Recebimento', icon='fa-money')
    def registrar_recebimento_cartao_credito(self, cartao, numero_cartao, bandeira, valor, valor_parcelado, numero_parcelas, data_primeira_parcela):
        from fabrica.models.financeiro import RecebimentoVendaCartaoCredito, Movimentacao, EspeciePagamento, CartaoCredito
        data = datetime.datetime.now()

        if not valor and not valor_parcelado:
            raise ValidationError('Informe o valor a ser recebido à vista ou parcelado')

        if not cartao:
            if not numero_cartao or not bandeira:
                raise ValidationError('Informe o número e a bandeira do cartão')
            cartao = CartaoCredito.objects.create(numero=numero_cartao, bandeira=bandeira)
            self.comprador.cartoes.add(cartao)

        if valor_parcelado:
            if not numero_parcelas or not data_primeira_parcela:
                raise ValidationError('Informe os número de parcelas e a data da primeira parcela.')
            if numero_parcelas < 1:
                raise ValidationError('O número de parcelas deve ser maior que 1')

        if valor:
            RecebimentoVendaCartaoCredito(venda=self, data=data, valor=valor, cartao_credito=cartao).save()
            #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CARTAO_CREDITO, descricao='Venda', data=data, valor=valor)
        if valor_parcelado:
            valor_parcela = valor_parcelado / numero_parcelas
            while numero_parcelas > 0:
                numero_parcelas -= 1
                RecebimentoVendaCartaoCredito(venda=self, data=data_primeira_parcela, valor=valor_parcela, cartao_credito=cartao).save()
                #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CARTAO_CREDITO, descricao='Venda', data=data_primeira_parcela, valor=valor_parcela)
                data_primeira_parcela = add_months(data_primeira_parcela, 1)

    def registrar_recebimento_cartao_credito_choices(self):
        return dict(cartao=self.comprador.cartoes.all())

    @action('Cheque', input='ReceberChequeForm', condition='pode_registrar_pagamento', category='Registrar Recebimento', icon='fa-money')
    def registrar_recebimento_cheque(self, conta, banco, numero_conta, numero_agencia, valor, quantidade_cheques, intervalo_dias, numero_primeiro_cheque, data_primeiro_cheque):
        from fabrica.models.financeiro import RecebimentoVendaCheque, Movimentacao, EspeciePagamento, ContaBancaria

        if not conta:
            if not banco or not numero_conta or not numero_agencia:
                raise ValidationError('Selecione o banco e informe os números da conta e da agência')
            conta = ContaBancaria.objects.create(banco=banco, numero_conta=numero_conta, numero_agencia=numero_agencia)
            self.comprador.contas.add(conta)

        if quantidade_cheques < 1:
            raise ValidationError('A quantidade de cheques deve ser maior ou igual a 1.')

        valor = valor/quantidade_cheques
        RecebimentoVendaCheque(venda=self, data=data_primeiro_cheque, valor=valor, conta=conta, numero_cheque=numero_primeiro_cheque).save()
        #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CHEQUE, descricao='Venda', data=data_primeiro_cheque, valor=valor)

        while quantidade_cheques > 1:
            quantidade_cheques -= 1
            if intervalo_dias == 30:
                data_primeiro_cheque = add_months(data_primeiro_cheque, 1)
            else:
                data_primeiro_cheque = add_days(data_primeiro_cheque, intervalo_dias)
            numero_primeiro_cheque += 1
            RecebimentoVendaCheque(venda=self, data=data_primeiro_cheque, valor=valor, conta=conta, numero_cheque=numero_primeiro_cheque).save()
            #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CHEQUE, descricao='Venda', data=data_primeiro_cheque, valor=valor)

    def registrar_recebimento_cheque_choices(self):
        return dict(conta=self.comprador.contas.all())

    @action('Cartão de Débito', input='ReceberCartaoDebitoForm', condition='pode_registrar_pagamento', category='Registrar Recebimento', icon='fa-money')
    def registrar_recebimento_cartacao_debito(self, conta, banco, numero_conta, numero_agencia, valor):
        from fabrica.models.financeiro import RecebimentoVendaCartaoDebito, Movimentacao, EspeciePagamento, ContaBancaria
        data = datetime.datetime.now()

        if not conta:
            if not banco or not numero_conta or not numero_agencia:
                raise ValidationError('Selecione o banco e informe os números da conta e da agência')
            conta = ContaBancaria.objects.create(banco=banco, numero_conta=numero_conta, numero_agencia=numero_agencia)
            self.comprador.contas.add(conta)

        RecebimentoVendaCartaoDebito(venda=self, data=data, valor=valor, conta=conta).save()
        #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CARTAO_DEBITO, descricao='Venda', data=data, valor=valor)

    def registrar_recebimento_cartacao_debito_choices(self):
        return dict(conta=self.comprador.contas.all())

    @action('Transferência/Depósito', input='ReceberTransferenciaBancariaForm', condition='pode_registrar_pagamento', category='Registrar Recebimento', icon='fa-money')
    def registrar_recebimento_transferencia_bancaria(self, conta, valor, data_operacao, numero_documento):
        from fabrica.models.financeiro import RecebimentoVendaTransferenciaBancaria, Movimentacao, EspeciePagamento, ContaBancaria
        data = datetime.datetime.now()

        RecebimentoVendaTransferenciaBancaria(venda=self, data=data, valor=valor, conta=conta, data_operacao=data_operacao, numero_documento=numero_documento).save()
        #Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.CARTAO_DEBITO, descricao='Venda', data=data, valor=valor)

    def registrar_recebimento_transferencia_bancaria_choices(self):
        return dict(conta=self.caixa.loja.contas.all())

    @action('Finalizar Pagamento', condition='pode_finalizar_pagamento')
    def finalizar_pagamento(self):
        from fabrica.models.financeiro import Movimentacao, EspeciePagamento
        for recebimento in self.recebimentovenda_set.all():
            Movimentacao.objects.create(caixa=self.caixa, especie=recebimento.especie, descricao='Venda', data=recebimento.data, valor=recebimento.valor, venda=self)
        troco = self.get_troco()
        if troco:
            Movimentacao.objects.create(caixa=self.caixa, especie_id=EspeciePagamento.DINHEIRO, descricao='Troco', data=datetime.datetime.now(), valor=-troco, venda=self)

    def pode_finalizar_pagamento(self):
        return self.itemvenda_set.exists() and not self.pagamento_foi_finalizado() and self.get_valor_recebido() >= self.get_valor_total()

    def pagamento_foi_finalizado(self):
        from fabrica.models.financeiro import Movimentacao
        return Movimentacao.objects.filter(venda=self).exists()


class ItemVenda(models.Model):
    venda = models.ForeignKey(Venda, composition=True)
    proprietario = models.ForeignKey(PessoaFisica, related_name='venda_proprietario_set')
    veiculo = models.ForeignKey(Veiculo, verbose_name='Veículo')
    kit_loja = models.ForeignKey('fabrica.KitLoja', verbose_name='Kit', lazy=True)
    desconto = models.DecimalField(verbose_name='Desconto', default=Decimal(0))

    fieldsets = (('', {'fields': ('venda', 'proprietario', 'veiculo', 'kit_loja', 'desconto')}),)

    class Meta:
        verbose_name = 'Item de Venda'
        verbose_name_plural = 'Itens'
        can_add_by_unit = 'Vendedor'
        can_list_by_unit = 'Vendedor', 'Gerente de Loja'
        add_form = 'ItemVendaForm'
        list_display = 'kit_loja', 'proprietario', 'veiculo', 'desconto', 'get_subtotal', 'is_impresso'
        list_lookups = 'venda__caixa__loja'
        usecase = 31

    def __str__(self):
        return 'Item #{} da venda #{}'.format(self.id, self.venda.id)

    @meta('Impresso')
    def is_impresso(self):
        return not self.solicitacaoimpressao_set.filter(produto=None).exists()

    def save(self, *args, **kwargs):
        super(ItemVenda, self).save(*args, **kwargs)
        if not SolicitacaoImpressao.objects.filter(item_venda=self).exists():
            for tipo_produto in self.kit_loja.kit.tipos_produtos.all():
                SolicitacaoImpressao.objects.create(item_venda=self, tipo_produto=tipo_produto)

    def delete(self, *args, **kwargs):
        for solicitacao in self.solicitacaoimpressao_set.order_by('-posicao'):
            solicitacao.delete()
        super(ItemVenda, self).delete(*args, **kwargs)

    @meta('Subtotal')
    def get_subtotal(self):
        return self.kit_loja.preco - self.desconto


class SolicitacaoImpressaoManager(models.DefaultManager):

    @subset('Aguardando Impressão', can_view=('Vendedor', 'Gerente de Loja'), can_notify=True)
    def all(self, *args, **kwargs):
        return super(SolicitacaoImpressaoManager, self).all(*args, **kwargs).exclude(posicao=None)

    @subset('Impressos')
    def todos(self):
        return self.filter(posicao=None)


class SolicitacaoImpressao(models.Model):
    item_venda = models.ForeignKey(ItemVenda, verbose_name='Item de Venda')
    tipo_produto = models.ForeignKey('fabrica.TipoProduto', verbose_name='Tipo de Produto', null=True)
    categoria_veiculo = models.ForeignKey('fabrica.CategoriaVeiculo', verbose_name='Categoria de Veículo', null=True)
    produto = models.ForeignKey('fabrica.Produto', verbose_name='Produto', null=True)
    posicao = models.IntegerField(verbose_name='Posição', null=True)
    data_impressao = models.DateTimeField(verbose_name='Data da Impressão', null=True)

    class Meta:
        verbose_name = 'Solicitação de Impressão'
        verbose_name_plural = 'Fila de Impressão'
        menu = 'Comunicação::Fila de Impressão', 'fa-external-link'
        can_list_by_unit = 'Vendedor', 'Gerente de Loja'
        icon = 'fa-reorder'
        list_display = ('posicao', 'tipo_produto', 'get_placa', 'get_venda', 'data_impressao')
        ordering = 'posicao',
        list_lookups = 'item_venda__venda__caixa__loja'
        list_shortcut = True

    def __str__(self):
        return '{} - {}'.format(self.tipo_produto, self.get_placa())

    @meta('Venda')
    def get_venda(self):
        return self.item_venda.venda

    @meta('Placa')
    def get_placa(self):
        return self.item_venda.veiculo.placa

    def pode_ser_priorizado(self):
        return self.posicao not in [1, None]

    # @action('Priorizar', condition='pode_ser_priorizado', inline=True)
    def priorizar(self):
        for i, solicitacao in enumerate(SolicitacaoImpressao.objects.exclude(pk=self.pk).exclude(posicao=None)):
            solicitacao.posicao = i+2
            solicitacao.save()
        self.posicao = 1
        self.save()

    @action('Simular Impressão', condition='pode_simular_impressao', inline=True)
    def simular_impressao(self, produto):

        if produto.tipo_produto.pk not in self.item_venda.kit_loja.kit.tipos_produtos.all().values_list('pk', flat=True):
            raise ValidationError('A blank deve ser do tipo "{}".'.format(produto.tipo_produto))

        posicao = self.posicao
        self.produto = produto
        self.posicao = None
        self.data_impressao = datetime.datetime.now()
        self.save()

        for solicitacao in SolicitacaoImpressao.objects.exclude(posicao__isnull=True):
            solicitacao.posicao = solicitacao.posicao - 1
            solicitacao.save()

        if SolicitacaoImpressao.objects.filter(produto=None, item_venda__venda=self.item_venda.venda).exists():
            self.item_venda.venda.data_impressao = None
        else:
            self.item_venda.venda.data_impressao = datetime.datetime.now()

        self.item_venda.venda.save()

    def pode_simular_impressao(self):
        return self.posicao == 1

    def simular_impressao_choices(self):
        qs = self.item_venda.venda.caixa.loja.produto_set.all() # filter(tipo_produto=self.tipo_produto)
        qs = qs.exclude(transferenciaproduto__data_recebimento__isnull=True)
        qs = qs.exclude(solicitacaoimpressao__isnull=False)
        return dict(produto=qs)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.posicao = SolicitacaoImpressao.objects.filter(item_venda__venda__caixa__loja=self.item_venda.venda.caixa.loja).exclude(posicao=None).count()+1
        super(SolicitacaoImpressao, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        posicao = self.posicao
        super(SolicitacaoImpressao, self).delete(*args, **kwargs)
        if posicao:
            for solicitacao in SolicitacaoImpressao.objects.filter(posicao__gt=posicao):
                solicitacao.posicao = solicitacao.posicao - 1
                solicitacao.save()