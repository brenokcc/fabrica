# -*- coding: utf-8 -*-

from django.db.transaction import atomic
from djangoplus.db import models
from djangoplus.decorators import action, subset, meta
import datetime


class TransferenciaProdutoManager(models.DefaultManager):

    @subset('Não-Recebidas pela Loja', can_view='Gerente de Loja', can_notify=True)
    def nao_recebidas_pela_loja(self):
        return self.filter(data_recebimento__isnull=True)

    @subset('Não-Recebidas pela Fábrica', can_view=('Gerente de Produção', 'Gerente de Escritório'), can_notify=True)
    def nao_recebidas_pela_fabrica(self):
        return self.filter(destino__isnull=True, data_recebimento__isnull=True)


class TransferenciaProduto(models.Model):

    descricao = models.CharField(verbose_name='Descrição', null=False, blank=False, search=True)
    data = models.DateField(verbose_name='Data', default=datetime.date.today, filter=True, exclude=True)
    origem = models.ForeignKey('fabrica.Loja', null=True, verbose_name='Origem', related_name='transferenciaorigem_set', blank=True, help_text='Deixe esse campo em branco caso esteja enviando da fábrica.')
    destino = models.ForeignKey('fabrica.Loja', null=True, verbose_name='Destino', related_name='transferenciadestino_set', ignore_lookup=True, filter=True, blank=True, help_text='Deixe esse campo em branco caso deseje enviar para a fábrica.')
    produtos = models.ManyToManyField('fabrica.Produto', verbose_name='Produtos', exclude=True, filter=True)
    data_recebimento = models.DateField(verbose_name='Data do Recebimento dos Produtos', exclude=True, null=True, filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('descricao', 'data'), ('origem', 'destino')), 'extra': ('get_quantitativo',)}),
        ('Produtos', {'relations': ('produtos',), 'actions': ('adicionar_produto_transferencia',)}),
        ('Recebimento', {'fields': ('data_recebimento',)}),
    )

    class Meta:
        verbose_name = 'Transferência de Produto'
        verbose_name_plural = 'Transferências'
        menu = 'Movimentações::Transferências de Produtos'
        verbose_female = True
        add_message = 'Adicione os produtos'
        can_admin = 'Gerente de Produção'
        list_lookups = 'destino', 'origem'
        can_list_by_unit = 'Gerente de Loja'
        can_add_by_unit = 'Gerente de Loja',
        can_list_by_organization = 'Gerente de Escritório'
        can_add_by_organization = 'Gerente de Escritório'
        icon = 'fa-exchange'
        add_shortcut = True

    def __str__(self):
        return 'Transferência #{} - {}'.format(self.pk, self.descricao)

    def can_edit(self):
        return not self.data_recebimento

    def can_delete(self):
        return self.can_edit()

    def pode_registrar_recebimento(self):
        if not self.data_recebimento and self.produtos.exists():
            if self._user:
                if self.destino:
                    return self.destino.pk in self._user.units()
                else:
                    return self._user.in_group('Gerente de Produção', 'Gerente de Escritório')
            else:
                return True
        else:
            return False

    def pode_adicionar_produtos(self):
        if self._user:
            if self.origem:
                return self.origem.pk in self._user.units('Gerente de Loja')
            else:
                return self._user.in_group('Gerente de Produção', 'Gerente de Escritório')
        else:
            return True

    def delete(self, *args, **kwargs):
        self.produtos.all().update(loja=None)
        super(TransferenciaProduto, self).delete(*args, **kwargs)

    @meta('Quantitativo', formatter='statistics')
    def get_quantitativo(self):
        return self.produtos.all().count('tipo_produto')


class PedidoManager(models.DefaultManager):

    @subset('Aguardando Envio para Produção', can_view='Gerente de Loja', can_notify=True)
    def iniciados(self):
        return self.filter(data_cancelamento__isnull=True, data_solicitacao__isnull=True)

    @subset('Aguardando Avaliação', can_view=('Gerente de Escritório',), can_notify=True)
    def aguardando_avaliacao(self):
        return self.filter(data_cancelamento__isnull=True, data_solicitacao__isnull=False, data_avaliacao__isnull=True)

    @subset('Aguardando Aprovação', can_view=('Gerente de Escritório da Fábrica',), can_notify=True)
    def aguardando_aprovacao(self):
        return self.filter(data_cancelamento__isnull=True, data_avaliacao__isnull=False, data_aprovacao__isnull=True)

    @subset('Aguardando Envio para Loja', can_view=('Gerente de Produção',), can_notify=True)
    def aguardando_envio_para_loja(self):
        return self.filter(data_cancelamento__isnull=True, data_aprovacao__isnull=False, data_envio_produtos__isnull=True)

    @subset('Aguardando Recebimento da Loja', can_view=('Gerente de Loja', 'Gerente de Produção'))
    def aguardando_recebimento_da_loja(self):
        return self.filter(data_cancelamento__isnull=True, data_envio_produtos__isnull=False, data_aprovacao__isnull=False)

    @subset('Concluídos', can_view=('Gerente de Escritório', 'Gerente de Produção', 'Gerente de Loja'))
    def concluidos(self):
        return self.filter(data_cancelamento__isnull=True, data_recebimento_produtos__isnull=False)

    @subset('Não-Embalados', can_view=('Funcionário da Fábrica',), can_notify=True)
    def nao_embalados(self):
        pks = []
        for pedido in self.aguardando_envio_para_loja():
            quantidade_nao_adicionada, tipo_produto = pedido.get_proximo_item_para_atendimento()
            if quantidade_nao_adicionada:
                pks.append(pedido.pk)
        return self.filter(pk__in=pks)

    @subset('Cancelados', can_view=('Gerente de Escritório', 'Gerente de Produção', 'Gerente de Loja'))
    def cancelados(self):
        return self.filter(data_cancelamento__isnull=False)


class Pedido(models.Model):

    id = models.AutoField(verbose_name='Número', primary_key=True)
    observacao = models.TextField(verbose_name='Observação', blank=True, default='')
    data_solicitacao = models.DateField(verbose_name='Data da Solicitação', null=True, exclude=True, filter=True)
    data_avaliacao = models.DateField(verbose_name='Data da Avaliação', null=True, exclude=True)
    data_aprovacao = models.DateField(verbose_name='Data da Aprovação', null=True, exclude=True)
    data_envio_produtos = models.DateField(verbose_name='Data do Envio dos Produtos', null=True, exclude=True)
    data_recebimento_produtos = models.DateField(verbose_name='Data do Recebimento dos Produtos', null=True, exclude=True)

    data_cancelamento = models.DateField(verbose_name='Data do Cancelamento', exclude=True, null=True)
    motivo_cancelamento = models.TextField(verbose_name='Motivo do Cancelamento', exclude=True, null=True)

    produtos = models.ManyToManyField('fabrica.Produto', exclude=True)

    loja = models.ForeignKey('fabrica.Loja', verbose_name='Loja', filter=True, search=('nome',))

    fieldsets = (('Status do Pedido', {'extra': ('get_status',)}),
        ('Dados Gerais::Dados Gerais', {'fields': (('loja', 'data_solicitacao'), 'observacao', ('data_avaliacao', 'data_aprovacao'), ('data_envio_produtos', 'data_recebimento_produtos'))}),
        ('Dados Gerais::Dados do Cancelamento', {'fields': ('data_cancelamento', 'motivo_cancelamento'), 'condition' : 'data_cancelamento'}),
        ('Dados Gerais::Itens do Pedido', {'inlines': ('itempedido_set', )}),
        ('Produtos::Produtos do Pedido', {'relations': ('get_produtos',), 'can_view':('Gerente de Produção', 'Gerente de Escritório da Fábrica', 'Funcionário da Fábrica', 'Gerente de Loja')}),
    )

    class Meta:
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'
        add_message = 'Por favor, adicione ou altere os produtos que desejar solicitar.'
        can_admin_by_unit = 'Gerente de Loja'
        can_list_by_organization = 'Gerente de Loja'
        can_admin_by_organization = 'Gerente de Escritório'
        list_display = 'id', 'data_solicitacao', 'data_avaliacao', 'data_envio_produtos', 'data_recebimento_produtos', 'loja'
        can_list = 'Gerente de Produção', 'Funcionário da Fábrica', 'Gerente de Escritório da Fábrica'
        icon = 'fa-shopping-cart'
        menu = 'Movimentações::Pedidos::Listar Pedidos', 'fa-exchange'
        add_shortcut = True
        list_shortcut = True
        list_lookups = 'loja'
        usecase = 17

    def __str__(self):
        return 'Pedido {} da loja {}'.format(self.pk, self.loja)

    @meta('Status', formatter='timeline')
    def get_status(self):
        if self.data_cancelamento:
            return [('Solicitado', self.data_solicitacao), ('Cancelado', self.data_cancelamento)]
        else:
            return [('Solicitado pela Loja', self.data_solicitacao), ('Avaliado pelo Escritório', self.data_avaliacao), ('Aprovado pela Fábrica', self.data_aprovacao), ('Em trânsito', self.data_envio_produtos), ('Recebido pela Loja', self.data_recebimento_produtos)]

    def get_status_atual(self):
        status = 'Iniciado', 'info'
        if self.data_cancelamento:
            return 'Cancelado', 'danger'
        if self.data_solicitacao:
            status = 'Solicitado', 'info'
        if self.data_avaliacao:
            status = 'Em Avaliação', 'info'
        if self.data_aprovacao:
            status = 'Em Atendimento', 'info'
        if self.data_envio_produtos:
            status = 'Em Trânsito', 'warning'
        if self.data_recebimento_produtos:
            status = 'Concluído', 'success'
        return status

    @meta('Produtos')
    def get_produtos(self):
        return self.produtos.all()

    @meta('Quantitativo', formatter='statistics')
    def get_quantitativo(self):
        return self.produtos.all().count('tipo_produto')

    def pode_enviar_para_producao(self):
        return not self.data_solicitacao and self.itempedido_set.exists()

    @action('Enviar Pedido', can_execute='Gerente de Loja', condition='pode_enviar_para_producao', inline='iniciados', usecase=19)
    def enviar_para_producao(self):
        self.data_solicitacao = datetime.date.today()
        self.save()
        if self.loja.pode_enviar_pedido:
            self.enviar_para_fabrica()

    @action('Cancelar Solicitação', can_execute='Gerente de Loja', condition='pode_cancelar_envio_para_producao', style='ajax', category=None, icon='fa-times')
    def cancelar_envio_para_producao(self):
        self.data_solicitacao = None
        self.save()

    def pode_cancelar_envio_para_producao(self):
        return not self.data_cancelamento and self.data_solicitacao and not self.data_avaliacao

    # inline
    @action('Enviar para Fábrica', can_execute='Gerente de Escritório', condition='pode_enviar_para_fabrica', inline='aguardando_avaliacao', usecase=21, category=None)
    def enviar_para_fabrica(self):
        self.data_avaliacao = datetime.date.today()
        self.save()

    def pode_enviar_para_fabrica(self):
        return not self.data_cancelamento and self.data_solicitacao and not self.data_avaliacao

    # inline
    @action('Cancelar Envio para Fábrica', can_execute='Gerente de Escritório', condition='pode_cancelar_envio_para_fabrica')
    def cancelar_envio_para_fabrica(self):
        self.data_avaliacao = None
        self.save()

    def pode_cancelar_envio_para_fabrica(self):
        return not self.data_cancelamento and self.data_avaliacao and not self.data_aprovacao

    @action('Cancelar Pedido', can_execute='Gerente de Escritório', condition='pode_ser_cancelado', category='Cancelamento', style='btn-danger popup')
    def cancelar(self, motivo_cancelamento):
        self.data_cancelamento = datetime.date.today()
        self.save()

    def pode_ser_cancelado(self):
        return not self.data_aprovacao and not self.data_cancelamento

    # inline
    @action('Aprovar Pedido', can_execute='Gerente de Escritório da Fábrica', condition='pode_aprovar_pedido', inline='aguardando_aprovacao', usecase=22)
    def aprovar_pedido(self):
        self.data_aprovacao = datetime.date.today()
        self.save()

    def pode_aprovar_pedido(self):
        return not self.data_aprovacao and not self.data_cancelamento

    # inline
    @action('Cancelar Aprovação', can_execute='Gerente de Escritório da Fábrica', condition='pode_cancelar_aprovacao', icon='fa-times')
    def cancelar_aprovacao(self):
        self.data_aprovacao = None
        self.save()

    def pode_cancelar_aprovacao(self):
        return not self.data_cancelamento and self.data_aprovacao and not self.produtos.exists()

    # inline
    @atomic
    @action('Enviar para Loja', can_execute='Gerente de Produção', condition='pode_enviar_para_loja', style='ajax', message='Imprima a guia de transferência e anexe aos produtos.', inline='aguardando_envio_para_loja', usecase=27)
    def enviar_para_loja(self):
        self.data_envio_produtos = datetime.date.today()
        if not self.data_avaliacao:
            self.data_avaliacao = datetime.date.today()
        self.save()

    def pode_enviar_para_loja(self):
        if not self.data_cancelamento and self.data_aprovacao and not self.data_envio_produtos:
            for item in self.itempedido_set.all():
                if not self.produtos.filter(tipo_produto=item.tipo_produto).count() == item.quantidade:
                    return False
            return True
        return False

    @atomic
    @action('Recomeçar Embalagem', can_execute=('Gerente de Produção', 'Funcionário da Fábrica'), condition='pode_adicionar_produtos', category=None, style='btn-danger ajax')
    def remover_produtos(self):
        self.produtos.update(loja=None)
        for produto in self.produtos.all():
            self.produtos.remove(produto)

    def pode_adicionar_produtos(self):
        return not self.data_cancelamento and self.data_aprovacao and not self.data_envio_produtos and not self.pode_enviar_para_loja()

    @action('Cancelar Envio para Loja', can_execute='Gerente de Produção', condition='pode_cancelar_envio_para_loja', category='2')
    def cancelar_envio_para_loja(self):
        self.data_envio_produtos = None
        self.save()

    def pode_cancelar_envio_para_loja(self):
        return not self.data_cancelamento and self.data_envio_produtos and not self.data_recebimento_produtos

    def pode_imprimir_guia(self):
        return not self.data_cancelamento and self.data_envio_produtos and not self.data_recebimento_produtos

    def pode_registrar_recebimento(self):
        return not self.data_cancelamento and self.data_envio_produtos and not self.data_recebimento_produtos

    def get_proximo_item_para_atendimento(self):
        for item in self.itempedido_set.all():
            quantidade_nao_adicionada = item.get_quantidade_nao_adicionada()
            if quantidade_nao_adicionada:
                return quantidade_nao_adicionada, item.tipo_produto
        return 0, None

    def save(self, *args, **kwargs):
        from fabrica.models.producao import TipoProduto
        super(Pedido, self).save(*args, **kwargs)
        if self.request:
            for pk, quantidade in self.request.GET.items():
                if pk.isdigit():
                    tipo_produto = TipoProduto.objects.get(pk=pk)
                    ItemPedido.objects.create(pedido=self, tipo_produto=tipo_produto, quantidade=int(quantidade))


class ItemPedido(models.Model):
    pedido = models.ForeignKey(Pedido, verbose_name='Pedido', composition=True)
    tipo_produto = models.ForeignKey('fabrica.TipoProduto', verbose_name='Tipo de Produto')
    quantidade = models.IntegerField(verbose_name='Quantidade Solicitada')
    observacao = models.TextField(verbose_name='Observação', null=True, exclude=True)

    class Meta:
        verbose_name = 'Item de Pedido'
        verbose_name_plural = 'Itens'
        can_admin_by_organization = 'Gerente de Escritório'
        can_admin_by_unit = 'Gerente de Loja'
        list_lookups = 'pedido__loja'
        list_display = 'tipo_produto', 'quantidade', 'get_quantidade_adicionada', 'observacao'
        usecase = 18

    @action('Alterar Quantidade', can_execute='Gerente de Escritório', condition='pode_alterar_quantidade', inline='aguardando_avaliacao', usecase=20)
    def alterar_quantidade(self, quantidade, observacao):
        self.save()

    @meta('Quantidade Atendida')
    def get_quantidade_adicionada(self):
        return self.pedido_id and self.pedido.produtos.filter(tipo_produto=self.tipo_produto).count() or 0

    def get_quantidade_nao_adicionada(self):
        return self.quantidade - self.get_quantidade_adicionada()

    def pode_alterar_quantidade(self):
        return not self.pedido.data_avaliacao

    def __str__(self):
        return 'Item #{}'.format(self.pk)

    def can_add(self):
        return self.pedido_id and not self.pedido.data_solicitacao or True

    def can_edit(self):
        return self.pedido_id and self.pedido.can_edit() or True

    def can_delete(self):
        return self.pedido_id and self.pedido.can_delete() or True

