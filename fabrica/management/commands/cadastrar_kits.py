# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from fabrica.models import CategoriaVeiculo, TipoProduto, Kit


class Command(BaseCommand):
    def handle(self, *args, **options):
        d = {'Placa dianteira de carro': ('Placa dianteira de carro',),
            'Placa traseira de carro': ('Placa traseira de carro',),
            'Tarjeta dianteira de carro': ('Tarjeta dianteira de carro',),
            'Tarjeta traseira de carro': ('Tarjeta traseira de carro',),
            'Placa de moto': ('Placa de moto',),
            'Tarjeta de moto': ('Tarjeta de moto',),
            'Kit par de placas': ('Placa dianteira de carro','Placa traseira de carro','Tarjeta dianteira de carro','Tarjeta traseira de carro',),
            'Kit placa dianteira de carro': ('Placa dianteira de carro','Tarjeta dianteira de carro'),
            'Kit placa traseira de carro': ('Placa traseira de carro','Tarjeta traseira de carro',),
            'Kit par de tarjeta': ('Tarjeta dianteira de carro','Tarjeta traseira de carro',),
            'Kit placa de moto': ('Placa de moto','Tarjeta de moto',),
        }
        for categoria_veiculo in CategoriaVeiculo.objects.all():
            for descricao, descricoes in d.items():
                kit = Kit.objects.get_or_create(descricao=descricao, categoria_veiculo=categoria_veiculo)[0]
                for tipo_produto in TipoProduto.objects.filter(categoria_veiculo=categoria_veiculo, descricao__in=descricoes):
                    kit.tipos_produtos.add(tipo_produto)