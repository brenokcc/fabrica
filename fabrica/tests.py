# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase
from fabrica.models import Produto, Pedido, TransferenciaProduto


class AppTestCase(TestCase):

    def _test(self):
        User.objects.create_superuser('000.000.000-00', None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    # @testcase('', username='000.000.000-00')
    def cadastrar_cliente(self):
        self.click_menu('Empresas')
        self.click_button('Cadastrar')
        self.enter('CNPJ', '64.755.120/0001-81')
        self.enter('Razão Social', 'Autoplac')
        self.enter('Nome Fantasia', 'Autoplac')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    def cadastrar_administradorsistemico(self):
        self.click_menu('Sistêmico', 'Usuários', 'Administradores Sistêmicos')
        self.click_button('Cadastrar')
        self.enter('CPF', '655.857.455-14')
        self.enter('Nome', 'Administrador Sistêmico')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    def registrar_recebimento(self):
        pedido = Pedido.objects.all()[0]
        produto = Produto.objects.all()[0]
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Registrar Recebimento')
        self.open('/fabrica/registrar_recebimento_pedido/%s/?codigo=%s' % (pedido.pk, produto.codigo))
        self.open('/admin/')

    def enviar_para_loja(self):
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Enviar para Loja')

    def embalar_produtos(self):
        pedido = Pedido.objects.all()[0]
        produto = Produto.objects.all()[0]
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Embalar Produtos')
        self.open('/fabrica/embalar_produtos/%s/?codigo=%s' % (pedido.pk, produto.codigo))
        self.open('/admin/')

    def registrar_producao(self):
        produto = Produto.objects.all()[0]
        self.click_menu('Movimentações', 'Produção', 'Registrar Produção')
        self.open('/fabrica/registrar_producao/?codigo=%s' % produto.codigo)
        self.open('/admin/')

    def cadastrar_producao(self):
        self.click_menu('Movimentações', 'Produção', ' Produções')
        self.click_button('Cadastrar')
        self.choose('Tipo de Produto', 'Placa de Moto')
        self.choose('Tipo de Película', '3M')
        self.enter('Película Utilizada', '3')
        self.enter('Alumínio Utilizado', '3')
        self.enter('Quantidade Prevista', '1')
        self.click_button('Cadastrar')

    def cadastrar_entradamateriaprima(self):
        self.click_menu('Entradas Matéria-Prima')
        self.click_button('Cadastrar')
        self.choose('Fornecedor', 'Fornecedor Padrão')
        self.enter('Número da Nota Fiscal', '001')
        self.click_button('Cadastrar')

        self.look_at_panel('Matéria-Prima')
        self.click_button('Adicionar Item')
        self.look_at_popup_window()
        self.choose('Matéria-Prima', 'Alumínio')
        self.enter('Quantidade', '10')
        self.click_button('Adicionar')

        self.look_at_panel('Matéria-Prima')
        self.click_button('Adicionar Item')
        self.look_at_popup_window()
        self.choose('Matéria-Prima', '3M')
        self.enter('Quantidade', '10')
        self.click_button('Adicionar')

    def cadastrar_materiaprima(self):
        self.click_menu('Movimentações', 'Estoque', 'Visualizar Estoque de Matérias-Primas')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Alumínio')
        self.enter('Estoque Crítico', '0')
        self.click_button('Cadastrar')

        self.click_button('Cadastrar')
        self.enter('Descrição', '3M')
        self.enter('Estoque Crítico', '0')
        self.click_button('Cadastrar')

    def cadastrar_fornecedor(self):
        self.click_menu('Produção', 'Fornecedores')
        self.click_button('Cadastrar')
        self.enter('CNPJ', '55.916.765/0001-68')
        self.enter('Razão Social', 'Fornecedor Padrão')
        self.enter('Nome Fantasia', 'Fornecedor Padrão')
        self.click_button('Cadastrar')

    def avaliar_pedido(self):
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Enviar para Fábrica')

    def aprovar_pedido(self):
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Aprovar Pedido')

    def cadastrar_pedido(self):
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_button('Cadastrar')
        self.enter('Observação', 'Teste')
        self.click_button('Cadastrar')

        self.look_at_panel('Itens do Pedido')
        self.choose('Tipo', 'Placa de Moto')
        self.enter('Quantidade Solicitada', '1')
        self.click_button('Adicionar')

    def enviar_pedido(self):
        self.click_menu('Movimentações', 'Pedidos', 'Listar Pedidos')
        self.click_icon('Visualizar')
        self.click_button('Enviar Pedido')

    def cadastrar_gerenteescritorio(self):
        self.click_menu('Usuários', 'Gerentes de Escritório')
        self.click_button('Cadastrar')
        self.enter('CPF', '248.731.575-01')
        self.enter('Nome', 'Gerente de Escritório')
        self.enter('E-mail', '')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Cadastrar')

    def cadastrar_gerenteloja(self):
        self.click_menu('Gerentes de Loja')
        self.click_button('Cadastrar')
        self.enter('CPF', '586.191.755-80')
        self.enter('Nome', 'Gerente de Loja')
        self.enter('E-mail', '')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Cadastrar')

    def cadastrar_operadorproducao(self):
        self.click_menu('Operadores de Produção')
        self.click_button('Cadastrar')
        self.enter('CPF', '424.414.911-21')
        self.enter('Nome', 'Operador de Produção')
        self.click_button('Cadastrar')

    def cadastrar_funcionariofabrica(self):
        self.click_menu('Funcionários da Fábrica')
        self.click_button('Cadastrar')
        self.enter('CPF', '807.590.013-83')
        self.enter('Nome', 'Funcionário da Fábrica')
        self.click_button('Cadastrar')

    def cadastrar_gerenteproducao(self):
        self.click_menu('Sistêmico', 'Usuários', 'Gerentes de Produção')
        self.click_button('Cadastrar')
        self.enter('CPF', '614.636.326-06')
        self.enter('Nome', 'Gerente de Produção')
        self.enter('E-mail', '')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Cadastrar')

    def cadastrar_gerenteescritoriofabrica(self):
        self.click_menu('Gerentes de Escritório da Fábrica')
        self.click_button('Cadastrar')
        self.enter('CPF', '777.777.777-77')
        self.enter('Nome', 'Gerente de Escritório da Fábrica')
        self.enter('E-mail', '')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Cadastrar')

    def cadastrar_kit(self):
        self.click_menu('Movimentações', 'Kits de Produtos')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Placa de Moto')
        self.choose('Categoria', 'Particular')
        self.choose('Tipos de Produtos', 'Placa de Moto')
        self.click_button('Cadastrar')

    def cadastrar_tipoproduto(self):
        self.click_menu('Tipos de Produtos')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Placa de Moto')
        self.look_at_panel('Consumo de Matéria-Prima por Unidade')
        self.enter('Película', 2)
        self.enter('Alumínio', 2)
        self.look_at_panel('Despedício de Matéria-Prima por Unidade')
        self.enter('Película', 1)
        self.enter('Alumínio', 1)
        self.click_button('Cadastrar')

    def cadastrar_loja(self):
        self.click_menu('Sistêmico', 'Loja', 'Lojas')
        self.click_button('Cadastrar')
        self.enter('CNPJ', '75.802.774/0001-35')
        self.enter('Nome Fantasia', 'Matriz')
        self.enter('Razão Social', 'Matriz')
        self.click_button('Cadastrar')

    def adicionar_cidade(self):
        self.click_menu('Estados')
        self.click_icon('Visualizar')
        self.look_at_panel('Municípios')
        self.click_button('Adicionar Município')
        self.look_at_popup_window()
        self.enter('Nome', 'Natal')
        self.choose('Estado', 'RN')
        self.enter('Código', '1')
        self.click_button('Adicionar')

    def cadastrar_estado(self):
        self.click_menu('Estados')
        self.click_button('Cadastrar')
        self.choose('País', 'Brasil')
        self.enter('Nome', 'Rio Grande do Norte')
        self.enter('Código', '1')
        self.click_button('Cadastrar')

    def cadastrar_pais(self):
        self.click_menu('Cadastros', 'Estados e Municípios', 'Paises')
        self.click_button('Cadastrar')
        self.choose('Nome', 'Brasil')
        self.enter('Código', '1')
        self.click_button('Cadastrar')

    def cadastrar_categoriaveiculo(self):
        self.click_menu('Movimentações', 'Categorias de Veículo')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Particular')
        self.enter('Descrição', 'A placa mais comum, para veículos particulares.')
        self.click_button('Cadastrar')

    def cadastrar_administrador(self):
        self.click_menu('Sistêmico', 'Empresas')
        self.click_icon('Visualizar')
        self.click_tab('Administração')
        self.click_button('Adicionar Administrador')
        self.look_at_popup_window()
        self.enter('CPF', '047.704.024-14')
        self.enter('Nome', 'Administrador')
        self.enter('E-mail', '')
        self.enter('Telefone Principal', '')
        self.enter('Telefone Secundário', '')
        self.click_button('Adicionar')

    def test_main(self):
        # from django.core.management import call_command
        # call_command('loaddata', 'fabrica/fixtures/_initial_data.json')

        self.create_superuser('000.000.000-00', 'senha')

        self.login('000.000.000-00', 'senha')
        self.cadastrar_administradorsistemico()
        self.cadastrar_categoriaveiculo()
        self.cadastrar_tipoproduto()
        self.logout()

        # administrador sistêmico
        self.login('655.857.455-14', 'senha')
        self.cadastrar_kit()
        self.cadastrar_gerenteproducao()
        self.cadastrar_gerenteescritoriofabrica()
        self.cadastrar_funcionariofabrica()
        self.cadastrar_operadorproducao()
        self.cadastrar_cliente()
        self.cadastrar_administrador()
        self.logout()

        # administrador
        self.login('047.704.024-14', 'senha')
        self.cadastrar_loja()
        self.cadastrar_gerenteescritorio()
        self.cadastrar_gerenteloja()
        self.logout()

        # gerente de loja
        self.login('586.191.755-80', 'senha')
        self.cadastrar_pedido()
        self.enviar_pedido()
        self.logout()

        # gerente de escritório
        self.login('248.731.575-01', 'senha')
        self.avaliar_pedido()
        self.logout()

        # gerente de escritório da fábrica
        self.login('777.777.777-77', 'senha')
        self.aprovar_pedido()
        self.logout()

        # gerente de produção
        self.login('614.636.326-06', 'senha')
        self.cadastrar_materiaprima()
        self.cadastrar_fornecedor()
        self.cadastrar_entradamateriaprima()
        self.cadastrar_producao()
        self.logout()

        # operador de produção
        self.login('424.414.911-21', 'senha')
        self.registrar_producao()
        self.logout()

        # funcionário da fábrica
        self.login('807.590.013-83', 'senha')
        self.embalar_produtos()
        self.logout()

        # gerente de produção
        self.login('614.636.326-06', 'senha')
        self.enviar_para_loja()
        self.logout()

        # gerente de loja
        self.login('586.191.755-80', 'senha')
        self.registrar_recebimento()

        # self.cadastrar_pais()
        # self.cadastrar_estado()
        # self.adicionar_cidade()
