# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from djangoplus.admin.views import index, public
from fabrica import views

urlpatterns = [
    url(r'^$', public),
    url(r'^admin/$', index),
    url(r'^webservice/$', views.webservice),
    url(r'', include('djangoplus.admin.urls')),
]
