# -*- coding: utf-8 -*-
import datetime, json
from django.views.decorators import csrf
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from fabrica.models import Venda, Produto, Pedido, TipoProduto, Producao, \
    ConfiguracaoEstoqueLoja, TransferenciaProduto, Loja, SolicitacaoImpressao, Cliente, OperadorPrensa, Prensa
from djangoplus.decorators.views import view, action, dashboard
from decimal import Decimal
from django.http.response import HttpResponse
from django.contrib import auth


@dashboard('Gerente de Loja', 'top')
def timelines(request):
    pedidos = []
    for pedido in Pedido.objects.filter(data_cancelamento__isnull=True).exclude(
            data_recebimento_produtos__isnull=False).all(request.user)[0:3]:
        pedido.request = request
        pedidos.append(pedido)

    return locals()


@view('Realizar Pedido', can_view='Gerente de Loja', icon='fa-shopping-cart', menu='Movimentações::Pedidos::Realizar Pedido')
def repor_estoque_loja(request):
    configuracoes = ConfiguracaoEstoqueLoja.objects.all(request.user)
    return locals()


@view('Visualizar Estoque', can_view='Gerente de Loja', icon='fa-th', menu='Movimentações::Estoque::Visualizar Estoque de Produtos')
def visualizar_estoque_loja(request):
    configuracoes = ConfiguracaoEstoqueLoja.objects.all(request.user)
    return locals()


@action(Producao, 'Imprimir Ordem de Produção', can_execute='Gerente de Produção', style='ajax pdf', inline='aguardando_producao', usecase=24)
def imprimir_ordem_producao(request, pk):
    obj = Producao.objects.get(pk=pk)
    primeiro_produto = obj.get_primeiro_produto()
    ultimo_produto = obj.get_ultimo_produto()
    hoje = datetime.date.today()
    return locals()


@action(Pedido, 'Imprimir Guia de Transferência', can_execute=['Gerente de Produção'], condition='pode_imprimir_guia', style='ajax pdf', inline='aguardando_recebimento_da_loja', usecase=28)
def imprimir_guia_transferencia(request, pk):
    pedido = Pedido.objects.get(pk=pk)
    report = pedido.get_quantitativo().as_table(request)
    hoje = datetime.date.today()
    return locals()


@action(TipoProduto, 'Cadastrar Produção', can_execute='Gerente de Produção', style='ajax', inline='aguardando_producao', usecase=23)
def nova_producao(request, pk):
    tipo_produto = TipoProduto.objects.get(pk=pk)
    return httprr(request, '/add/fabrica/producao/?tipo_produto={}&quantidade_solicitada={}'.format(
        tipo_produto.pk, tipo_produto.get_reposicao_necessaria()))


@action(TransferenciaProduto, 'Imprimir Guia', can_execute='Gerente de Escritório', style='ajax pdf')
def imprimir_transferencia(request, pk):
    transferencia = TransferenciaProduto.objects.get(pk=pk)
    report = transferencia.get_quantitativo().as_table(request)
    loja = transferencia.destino
    hoje = datetime.date.today()
    return locals()


@action(Venda, 'Imprimir Venda', style='ajax pdf', category=None, icon='fa-print', can_execute='Vendedor')
def imprimir_venda(request, pk):
    venda = Venda.objects.get(pk=pk)
    loja = venda.caixa.loja
    hoje = datetime.date.today()
    vias = range(0, 2)
    return locals()

# @view('Relatório PDF', menu='Relatório', style='')
# def relatorio(request):
#    return locals()


@view('Registrar Produção', can_view='Operador de Produção', icon='fa-barcode', menu='Movimentações::Produção::Registrar Produção', style='', shortcut=True, usecase=25)
def registrar_producao(request):
    url = '/fabrica/registrar_producao/'
    voltar_url = '/admin/'
    title = 'Escaner de Produção'
    codigo = request.GET.get('codigo')
    if codigo:
        qs = Produto.objects.filter(codigo=codigo)
        if qs.exists():
            Produto.objects.filter(codigo=codigo).update(produzido=True)
            produto = qs[0]
            produto.producao.quantidade_produzida = Produto.objects.filter(producao=produto.producao,
                                                                           produzido=True).count()
            produto.producao.save()
            diferenca = produto.producao.quantidade_solicitada - produto.producao.quantidade_produzida
            if diferenca > 0:
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Falta(m) {} produto(s) para concluir o registro da produção.'.format(diferenca),
                str(produto.producao), 'alert-info')
            else:
                produto.producao.data_conclusao = datetime.date.today()
                produto.producao.save()
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Registro de produção concluída com sucesso.', str(produto.producao), 'alert-success')
            return HttpResponse(resposta)
        else:
            resposta = '{}::{}::{}::{}'.format(codigo, 'Produto não encontrado.', '', 'alert-danger')
            return HttpResponse(resposta)
    return locals()


@action(Pedido, 'Embalar Produtos', can_execute=('Funcionário da Fábrica', 'Gerente de Produção', ),
        style='', condition='pode_adicionar_produtos', inline='aguardando_envio_para_loja', usecase=26)
def embalar_produtos(request, pk):
    pedido = Pedido.objects.get(pk=pk)
    url = '/fabrica/embalar_produtos/{}/'.format(pedido.pk)
    voltar_url = '/view/fabrica/pedido/{}/'.format(pedido.pk)
    title = 'Adicionar Produto - {}'.format(pedido)
    codigo = request.GET.get('codigo')
    quantidade_nao_adicionada, tipo_produto = pedido.get_proximo_item_para_atendimento()
    if codigo:
        qs = Produto.objects.filter(codigo=codigo, produzido=True)
        if qs.exists():
            produto = qs[0]
            if produto.tipo_produto == tipo_produto:
                if not produto.loja:
                    # produto.loja=pedido.loja
                    # produto.save()
                    pedido.produtos.add(produto)
                    quantidade_nao_adicionada, tipo_produto = pedido.get_proximo_item_para_atendimento()
                    if quantidade_nao_adicionada:
                        msg = 'Por favor, adicione mais {} produto(s) do tipo {}'.format(
                        quantidade_nao_adicionada, tipo_produto)
                        resposta = '{}::{}::{}::{}'.format(codigo, msg, str(pedido), 'alert-info')
                    else:
                        resposta = '{}::{}::{}::{}'.format(
                        codigo, 'Parabéns! Todos os itens do pedido foram adicionados.', str(pedido),
                        'alert-success')
                else:
                    resposta = '{}::{}::{}::{}'.format(codigo, 'Esse produto já foi adicionado.', '', 'alert-danger')
            else:
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Por favor, adicione "{}" ao invés de "{}".'.format(tipo_produto, produto.tipo_produto), '',
                'alert-danger')
            return HttpResponse(resposta)
        else:
            resposta = '{}::{}::{}::{}'.format(codigo, 'Produto não encontrado.', '', 'alert-danger')
            return HttpResponse(resposta)
    else:
        msg = 'Por favor adicione {} produto(s) do tipo {}'.format(quantidade_nao_adicionada, tipo_produto)
    return locals()


@action(TransferenciaProduto, 'Embalar Produtos', can_execute=('Gerente de Escritório', 'Gerente de Loja'),
        style='', condition='pode_adicionar_produtos', inline=True)
def adicionar_produto_transferencia(request, pk):
    transferencia = TransferenciaProduto.objects.get(pk=pk)
    url = '/fabrica/adicionar_produto_transferencia/{}/'.format(transferencia.pk)
    voltar_url = '/view/fabrica/transferenciaproduto/{}/'.format(transferencia.pk)
    title = 'Adicionar Produto - {}'.format(transferencia)
    codigo = request.GET.get('codigo')
    if codigo:
        qs = Produto.objects.filter(codigo=codigo, loja=transferencia.origem)
        if qs.exists():
            transferencia.produtos.add(qs[0])
            resposta = '{}::{}::{}::{}'.format(
            codigo, 'Produto adicionado com sucesso. Adicione mais produtos caso necessário.', str(transferencia),
            'alert-success')
            return HttpResponse(resposta)
        else:
            resposta = '{}::{}::{}::{}'.format(codigo, 'Produto não encontrado.', '', 'alert-danger')
            return HttpResponse(resposta)
    else:
        msg = 'Por favor adicione os produto(s) a serem transferidos'
    return locals()


@action(Pedido, 'Registrar Recebimento',
        can_execute=['Gerente de Loja',], style='',
        condition='pode_registrar_recebimento', category=None, inline=True, usecase=29)
def registrar_recebimento_pedido(request, pk):
    pedido = Pedido.objects.get(pk=pk)
    url = '/fabrica/registrar_recebimento_pedido/{}/'.format(pedido.pk)
    voltar_url = '/view/fabrica/pedido/{}/'.format(pedido.pk)
    title = 'Registrar Recebimento de Produto - {}'.format(pedido)
    codigo = request.GET.get('codigo')
    if codigo:
        qs = pedido.produtos.filter(codigo=codigo)
        if qs.exists():
            produto = qs[0]
            produto.loja = pedido.loja
            produto.save()
            remanescente = pedido.produtos.filter(loja__isnull=True).count()
            if remanescente:
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Recebimento registrado com sucesso. Ainda falta registrar {} produto(s).'.format(remanescente),
                str(pedido), 'alert-info')
            else:
                pedido.data_recebimento_produtos = datetime.date.today()
                pedido.save()
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Parabéns! Você concluiu o registro de recebimento do produtos desse pedido.',
                str(pedido), 'alert-success')
            return HttpResponse(resposta)
        else:
            resposta = '{}::{}::{}::{}'.format(codigo, 'Produto não encontrado.', '', 'alert-danger')
            return HttpResponse(resposta)
    else:
        msg = 'Por favor informe os produto(s) para registrar o recebimento.'
    return locals()


@action(TransferenciaProduto, 'Registrar Recebimento',
        can_execute=['Gerente de Loja', 'Gerente de Escritório', 'Gerente de Produção'], style='',
        condition='pode_registrar_recebimento', category=None, inline=True)
def registrar_recebimento(request, pk):
    transferencia = TransferenciaProduto.objects.get(pk=pk)
    url = '/fabrica/registrar_recebimento/{}/'.format(transferencia.pk)
    voltar_url = '/view/fabrica/transferenciaproduto/{}/'.format(transferencia.pk)
    title = 'Registrar Recebimento de Produto - {}'.format(transferencia)
    codigo = request.GET.get('codigo')
    if codigo:
        qs = transferencia.produtos.filter(codigo=codigo)
        if qs.exists():
            produto = qs[0]
            produto.loja = transferencia.destino
            produto.save()
            if transferencia.destino:
                remanescente = transferencia.produtos.filter(loja__isnull=True).count()
            else:
                remanescente = transferencia.produtos.filter(loja__isnull=False).count()
            if remanescente:
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Recebimento registrado com sucesso. Ainda falta registrar {} produto(s).'.format(remanescente),
                str(transferencia), 'alert-info')
            else:
                transferencia.data_recebimento = datetime.date.today()
                transferencia.save()
                resposta = '{}::{}::{}::{}'.format(
                codigo, 'Parabéns! Você concluiu o registro de recebimento do produtos dessa transferência.',
                str(transferencia), 'alert-success')
            return HttpResponse(resposta)
        else:
            resposta = '{}::{}::{}::{}'.format(codigo, 'Produto não encontrado.', '', 'alert-danger')
            return HttpResponse(resposta)
    else:
        msg = 'Por favor informe os produto(s) para registrar o recebimento.'
    return locals()


def calcular_producao_ajax(request, tipo_produto, pelicula, aluminio, quantidade_solicitada):
    tipo_produto = TipoProduto.objects.get(pk=tipo_produto)
    pelicula, aluminio, quantidade_solicitada = Decimal(pelicula), Decimal(aluminio), Decimal(quantidade_solicitada)
    if tipo_produto.consumo_pelicula and tipo_produto.consumo_aluminio:
        if quantidade_solicitada:
            pelicula = tipo_produto.consumo_pelicula * quantidade_solicitada
            aluminio = tipo_produto.consumo_aluminio * quantidade_solicitada
        elif pelicula:
            quantidade_solicitada = int(pelicula / tipo_produto.consumo_pelicula)
            aluminio = tipo_produto.consumo_aluminio * quantidade_solicitada
        elif aluminio:
            quantidade_solicitada = int(aluminio / tipo_produto.consumo_aluminio)
            pelicula = tipo_produto.consumo_pelicula * quantidade_solicitada
        s = ('{};{};{}'.format(pelicula, aluminio, quantidade_solicitada)).replace('.', ',')
    else:
        s = '0;0;0;0'
    return HttpResponse(s)

#curl --data  'op=0&usuario=233.246.794-78&senha=senha' http://localhost:8000/fabrica/webservice/
#curl --data  'op=1&id_matriz=1&id_loja=1&usuario=233.246.794-78&senha=senha' http://localhost:8000/fabrica/webservice/
#curl --data  'op=2&token=77de68daecd823babbb58edb1c8e14d7106e83bb' http://localhost:8000/fabrica/webservice/
#curl --data  'op=3&token=77de68daecd823babbb58edb1c8e14d7106e83bb&usuario=233.246.794-78&senha=senha' http://localhost:8000/fabrica/webservice/
#curl --data  'op=4&token=77de68daecd823babbb58edb1c8e14d7106e83bb&imagem=' http://localhost:8000/fabrica/webservice/
#curl --data  'op=5&token=77de68daecd823babbb58edb1c8e14d7106e83bb&item=7&alfanumerico=NNV-4775&qrcode=20161012000001' http://localhost:8000/fabrica/webservice/
#curl --data  'op=6&token=77de68daecd823babbb58edb1c8e14d7106e83bb&item=23&alfanumerico=NNV-4775&qrcode=20170306000005&usuario=233.246.794-78' http://localhost:8000/fabrica/webservice/

@csrf.csrf_exempt
def webservice(request):

    LISTAR_LOJAS = 0
    SOLICITAR_CADASTRO_PRENSA = 1
    LISTAR_PEDIDOS = 2
    AUTORIZACAO_POR_SENHA = 3
    AUTORIZACAO_POR_BIOMETRIA = 4
    AUTORIZACAO_IMPRESSAO = 5
    CONFIRMACAO_IMPRESSAO = 6

    op = int(request.POST.get('op', 9))

    #lista lojas
    if op == LISTAR_LOJAS:
        usuario = request.POST.get('usuario')
        senha = request.POST.get('senha')
        qs_operador = OperadorPrensa.objects.filter(cpf=usuario)
        if qs_operador.exists():
            if auth.authenticate(username=usuario, password=senha):
                pks = qs_operador.values_list('loja__cliente', flat=True).distinct()
                for cliente in Cliente.objects.filter(pk__in=pks):
                    lojas = []
                    for loja in cliente.loja_set.all():
                        lojas.append(dict(id=loja.pk, nome=str(loja)))
                    data = dict(sucesso=1, lojas=lojas, mensagem=None)
            else:
                data = dict(sucesso=0, lojas=[], mensagem='Usuario nao autenticado')
        else:
            data = dict(sucesso=0, lojas=[], mensagem='Usuario nao encontrado')
    elif op == SOLICITAR_CADASTRO_PRENSA:
        id_loja = request.POST.get('id_loja')
        usuario = request.POST.get('usuario')
        senha = request.POST.get('senha')
        qs_loja = Loja.objects.filter(pk=id_loja)
        qs_operador = OperadorPrensa.objects.filter(cpf=usuario)
        if qs_loja.exists() and qs_operador.exists():
            if auth.authenticate(username=usuario, password=senha):
                loja = qs_loja[0]
                qs_prensa = Prensa.objects.filter(loja=loja)
                if qs_prensa.exists():
                    prensa = qs_prensa[0]
                else:
                    prensa = Prensa()
                    prensa.loja = loja
                    prensa.descricao = 'Prensa da loja {} criada pelo usuário {}'.format(loja, usuario)
                    prensa.save()
                data = dict(token=prensa.token, sucesso=1, mensagem=None)
            else:
                data = dict(token=None, sucesso=0, mensagem='Usuario nao autenticado')
        else:
            data = dict(token=None, sucesso=0, mensagem='Loja ou operador nao encontrado')
    elif op == LISTAR_PEDIDOS:
        token = request.POST.get('token')
        qs_prensa = Prensa.objects.filter(token=token)
        itens = []
        if qs_prensa.exists():
            prensa = qs_prensa[0]
            qs_solicitacao = SolicitacaoImpressao.objects.filter(item_venda__venda__loja=prensa.loja, data_impressao__isnull=True).order_by('posicao')
            for solicitacao in qs_solicitacao[0:1]:
                itens.append(dict(item_id=solicitacao.pk,
                                  categoria=solicitacao.categoria_veiculo.pk,
                                  tipo=solicitacao.tipo_produto.pk,
                                  alfanumerico=solicitacao.item_venda.veiculo.placa,
                                  municipio=solicitacao.item_venda.veiculo.municipio.nome,
                                  estado=solicitacao.item_venda.veiculo.municipio.estado.sigla))
            data = dict(sucesso=1, mensagem=None, itens=itens)
        else:
            data = dict(sucesso=0, mensagem='Token invalido', itens=itens)

    elif op == AUTORIZACAO_POR_SENHA:
        token = request.POST.get('token')
        usuario = request.POST.get('usuario')
        senha = request.POST.get('senha')
        qs_prensa = Prensa.objects.filter(token=token)
        if qs_prensa:
            prensa = qs_prensa[0]
            qs_operador = OperadorPrensa.objects.filter(cpf=usuario, loja=prensa.loja)
            if qs_operador:
                if auth.authenticate(username=usuario, password=senha):
                    data = dict(sucesso=1, mensagem=None)
                else:
                    data = dict(sucesso=0, mensagem='Usuario nao autenticado')
            else:
                data = dict(sucesso=0, mensagem='Operador nao encontrado')
        else:
            data = dict(sucesso=0, mensagem='Token invalido')

    elif op == AUTORIZACAO_POR_BIOMETRIA:
        token = request.POST.get('token')
        imagem = request.POST.get('imagem')
        data = dict(sucesso=0, mensagem='Funciondalidade nao implementada')
    elif op == AUTORIZACAO_IMPRESSAO:
        token = request.POST.get('token')
        item = request.POST.get('item')
        alfanumerico = request.POST.get('alfanumerico')
        qrcode = request.POST.get('qrcode')
        qs_solicitacao = SolicitacaoImpressao.objects.filter(pk=item)
        qs_prensa = Prensa.objects.filter(token=token)
        if qs_prensa.exists():
            if qs_solicitacao.exists():
                solicitacao = qs_solicitacao[0]
                if solicitacao.item_venda.veiculo.placa == alfanumerico:
                    prensa = qs_prensa[0]
                    qs_produto = Produto.objects.filter(codigo=qrcode, loja=prensa.loja)
                    if qs_produto.exists():
                        produto = qs_produto[0]
                        if produto.tipo_produto == solicitacao.tipo_produto:
                            data = dict(sucesso=1, mensagem=None)
                        else:
                            data = dict(sucesso=0, mensagem='Categoria invalida')
                    else:
                        data = dict(sucesso=0, mensagem='Produto nao localizado no estoque')
                else:
                    data = dict(sucesso=0, mensagem='Alfanumerico nao confere')
            else:
                data = dict(sucesso=0, mensagem='Pedido inexistente')
        else:
            data = dict(sucesso=0, mensagem='Token invalido')
    elif op == CONFIRMACAO_IMPRESSAO:
        token = request.POST.get('token')
        item = request.POST.get('item')
        alfanumerico = request.POST.get('alfanumerico')
        qrcode = request.POST.get('qrcode')
        usuario = request.POST.get('usuario')
        qs_prensa = Prensa.objects.filter(token=token)
        qs_produto = Produto.objects.filter(codigo=qrcode)
        qs_solicitacao = SolicitacaoImpressao.objects.filter(pk=item, item_venda__veiculo__placa=alfanumerico)
        if qs_prensa.exists():
            prensa = qs_prensa[0]
            qs_operador = OperadorPrensa.objects.filter(cpf=usuario, loja=prensa.loja)
            if qs_operador.exists():
                if qs_produto.exists():
                    if qs_solicitacao.exists():
                        solicitacao = qs_solicitacao[0]
                        solicitacao.simular_impressao(qs_produto[0])
                        data = dict(sucesso=1, mensagem=None)
                    else:
                        data = dict(sucesso=0, mensagem='Pedido inexistente')
                else:
                    data = dict(sucesso=0, mensagem='Produto nao localizado no estoque')
            else:
                data = dict(sucesso=0, mensagem='Operador nao encontrado')
        else:
            data = dict(sucesso=0, mensagem='Token invalido')
    else:
        data = dict(sucesso=0, mensagem='Operacao invalida')

    s = json.dumps(data)

    #import datetime, os
    #log_dir = '/var/opt/fabrica/static/'
    #if os.path.exists(log_dir):
    #    l = []
    #    for key in request.POST:
    #        l.append('{}={}'.format(key, request.POST[key]))
    #    f = open('{}/webservice.txt'.format(log_dir), 'a')
    #    f.write('{}\nIn: {}\nOut:{}\n\n'.format(datetime.datetime.now(), ','.join(l), s))

    return HttpResponse(s)
