# -*- coding: utf-8 -*-

# Generated by Django 1.10 on 2016-10-12 19:55


from django.db import migrations
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fabrica', '0007_auto_20161012_1949'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='veiculo',
            name='cliente',
        ),
        migrations.AddField(
            model_name='veiculo',
            name='loja',
            field=djangoplus.db.models.fields.ModelChoiceField(default=1, on_delete=django.db.models.deletion.CASCADE, to='fabrica.Loja', verbose_name='Loja'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='pessoafisica',
            name='loja',
            field=djangoplus.db.models.fields.ModelChoiceField(on_delete=django.db.models.deletion.CASCADE, to='fabrica.Loja', verbose_name='Loja'),
        ),
    ]
