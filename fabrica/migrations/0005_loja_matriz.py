# -*- coding: utf-8 -*-

# Generated by Django 1.10 on 2016-10-12 19:11


from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fabrica', '0004_auto_20161012_1357'),
    ]

    operations = [
        migrations.AddField(
            model_name='loja',
            name='matriz',
            field=djangoplus.db.models.fields.BooleanField(default=False, help_text='Marque essa op\xe7\xe3o caso a loja seja a matriz', verbose_name='Matriz'),
        ),
    ]
