# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2017-07-29 13:56


from django.db import migrations, models
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fabrica', '0041_recebimentocartaodebito'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecebimentoVendaTransferenciaBancaria',
            fields=[
                ('recebimentovenda_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fabrica.RecebimentoVenda')),
                ('numero_documento', djangoplus.db.models.fields.IntegerField(verbose_name='N\xfamero do Documento')),
                ('conta', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fabrica.ContaBancaria')),
            ],
            options={
                'verbose_name': 'Recebimento por Transfer\xeancia Banc\xe1ria',
                'verbose_name_plural': 'Recebimentos por Transfer\xeancia Banc\xe1ria',
            },
            bases=('fabrica.recebimentovenda',),
        ),
        migrations.RenameModel(
            old_name='RecebimentoCartaoDebito',
            new_name='RecebimentoVendaCartaoDebito',
        ),
        migrations.AddField(
            model_name='loja',
            name='contas',
            field=djangoplus.db.models.fields.OneToManyField(to='fabrica.ContaBancaria', verbose_name=b'Contas Banc\xc3\xa1rias'),
        ),
    ]
