# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2017-07-02 10:09


from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fabrica', '0023_banco_bandeira_cartao_conta'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banco',
            name='numero',
            field=djangoplus.db.models.fields.CharField(max_length=255, verbose_name='N\xfamero'),
        ),
    ]
