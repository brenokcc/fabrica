
function calcular_producao(tipo_produto, tipo_pelicula, pelicula, aluminio, quantidade_solicitada){
    var tipo_produto = tipo_produto.val();
    var tipo_pelicula = tipo_pelicula.val().replace(',', '.');
    var valor_pelicula = pelicula.val().replace(',', '.')
    var valor_aluminio = aluminio.val().replace(',', '.')
    var valor_quantidade_solicitada = quantidade_solicitada.val().replace(',', '.')
    if(tipo_produto && tipo_pelicula) {
        if (tipo_pelicula == '') tipo_pelicula = 0;
        if (valor_pelicula == '') valor_pelicula = 0;
        if (valor_aluminio == '') valor_aluminio = 0;
        if (valor_quantidade_solicitada == '') valor_quantidade_solicitada = 0;

        var url = '/fabrica/calcular_producao_ajax/' + tipo_produto + '/' + valor_pelicula + '/' + valor_aluminio + '/' + valor_quantidade_solicitada + '/'
        $.ajax({
            url: url,
        }).done(function (html) {
            var tokens = html.split(';');
            pelicula.val(tokens[0]);
            aluminio.val(tokens[1]);
            quantidade_solicitada.val(tokens[2]);
        });
    } else {
        alert('Selecione o tipo de produto e o tipo de película.');
    }
}

function add__fabrica__producao(){
    var tipo_produto = $('#id_tipo_produto');
    var tipo_pelicula = $('#id_tipo_pelicula');
    var pelicula = $('#id_pelicula');
    var aluminio = $('#id_aluminio');
    var quantidade_solicitada = $('#id_quantidade_solicitada');

    pelicula.blur(function(){
        aluminio.val('');
        quantidade_solicitada.val('');
        calcular_producao(tipo_produto, tipo_pelicula, pelicula, aluminio, quantidade_solicitada);
    })
    aluminio.blur(function(){
        pelicula.val('');
        quantidade_solicitada.val('');
        calcular_producao(tipo_produto, tipo_pelicula, pelicula, aluminio, quantidade_solicitada);
    })
    quantidade_solicitada.blur(function(){
        pelicula.val('');
        aluminio.val('');
        calcular_producao(tipo_produto, tipo_pelicula, pelicula, aluminio, quantidade_solicitada);
    })
}

function add__fabrica__movimentacao(){
    var tipo_movimentacao = $('#id_tipo_movimentacao');
    function configurar_tipo_despesa(){
        var value = tipo_movimentacao.val();
        if(value==1){
            $('#id_tipo_despesa').parent().parent().parent().show();
        } else {
            $('#id_tipo_despesa').parent().parent().parent().hide();
            $('#id_tipo_despesa').val('');
        }
    }
    tipo_movimentacao.change(configurar_tipo_despesa);
    configurar_tipo_despesa();
}
