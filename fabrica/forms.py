# -*- coding: utf-8 -*-

from djangoplus.ui.components import forms
from fabrica.models import ItemVenda, TipoProduto, Produto, CategoriaVeiculo, KitLoja, Cliente, \
    Administrador
from fabrica.models.financeiro import Banco, ContaBancaria, CartaoCredito, Bandeira, ContaPagar


class ClienteForm(forms.ModelForm):

    class Meta:
        model = Cliente
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fieldsets = Cliente.fieldsets
        else:
            self.fields['cpf_administrador'] = forms.CpfField(label='CPF', required=False)
            self.fields['nome_administrador'] = forms.CharField(label='Nome', required=False)
            self.fields['email_administrador'] = forms.CharField(label='E-mail', required=False)
            self.fields['telefone_principal_administrador'] = forms.PhoneField(label='Telefone Principal', required=False)
            self.fields['telefone_secundario_administrador'] = forms.PhoneField(label='Telefone Secundário', required=False)
            self.fieldsets = Cliente.fieldsets + (
                ('Administrador', {'fields': (('cpf_administrador', 'nome_administrador'), 'email_administrador',
                ('telefone_principal_administrador', 'telefone_secundario_administrador'))}),
            )

    def clean(self):
        cpf = self.cleaned_data.get('cpf_administrador')
        nome = self.cleaned_data.get('nome_administrador')
        email = self.cleaned_data.get('email_administrador')
        if cpf or nome or email:
            if not (cpf and nome and email):
                raise forms.ValidationError('O CPF, nome e e-mail do administrador é obrigatório caso ele seja informado no cadastro do cliente.')
        return self.cleaned_data

    def save(self, *args, **kwargs):
        super(ClienteForm, self).save(*args, **kwargs)
        administrador = Administrador()
        administrador.cliente = self.instance
        administrador.cpf=self.cleaned_data.get('cpf_administrador')
        administrador.nome = self.cleaned_data.get('nome_administrador')
        administrador.email = self.cleaned_data.get('email_administrador')
        administrador.telefone_principal = self.cleaned_data.get('telefone_principal_administrador')
        administrador.telefone_secundario = self.cleaned_data.get('telefone_secundario_administrador')
        if administrador.cpf:
            administrador.save()


class RealizarPedido(forms.Form):
    class Meta:
        submit_label = 'Confirmar'
        title = 'Realizar Pedido'
    quantidade = forms.IntegerField(label='Quantidade')


class ItemVendaForm(forms.ModelForm):

    categoria_veiculo = forms.ModelChoiceField(CategoriaVeiculo.objects.all(), label='Categoria')
    kit_loja = forms.ModelChoiceField(queryset=KitLoja.objects.all(), label='Kit', form_filters=[['categoria_veiculo', 'kit__categoria_veiculo']])

    fieldsets = (('', {'fields': (('venda', 'categoria_veiculo', 'kit_loja', 'proprietario', 'veiculo', 'desconto'),)}),)

    class Meta:
        model = ItemVenda
        fields = ('proprietario', 'veiculo', 'kit_loja', 'desconto')
        submit_label = 'Adicionar Item'
        title = ''

    def __init__(self, *args, **kwargs):
        super(ItemVendaForm, self).__init__(*args, **kwargs)
        self.horizontal = False


class ReceberChequeForm(forms.Form):
    conta = forms.ModelChoiceField(ContaBancaria.objects.none(), label='Conta', required=False)

    banco = forms.ModelChoiceField(Banco.objects.all(), label='Banco', required=False)
    numero_conta = forms.OneDigitValidationField(label='Conta', required=False)
    numero_agencia = forms.OneDigitValidationField(label='Agência', required=False)

    valor = forms.DecimalField(label='Valor Total', required=True)
    quantidade_cheques = forms.IntegerField(label='Quantidade de Cheques', initial=1, required=True)
    intervalo_dias = forms.IntegerField(label='Intervalo entre as Parcelas', required=False, initial=30)

    numero_primeiro_cheque = forms.IntegerField(label='Nº do Primeiro Cheque', required=True)
    data_primeiro_cheque = forms.DateField(label='Data do Primeiro Cheque', required=True)

    fieldsets = (
        ('Conta Previamente Cadastrada', {'fields': ('conta',)}),
        ('Cadastrar Nova Conta', {'fields': (('banco', 'numero_conta', 'numero_agencia'),)}),
        ('Dados do Pagamento', {'fields': (('valor', 'numero_primeiro_cheque', 'data_primeiro_cheque'), ('quantidade_cheques', 'intervalo_dias'),)}))


class ReceberCartaoCreditoForm(forms.Form):
    cartao = forms.ModelChoiceField(CartaoCredito.objects.none(), label='Cartão', required=False)

    numero_cartao = forms.CreditCardField(label='Número', required=False)
    bandeira = forms.ModelChoiceField(Bandeira.objects.all(), label='Bandeira', required=False)

    valor = forms.DecimalField(label='Valor à Vista', required=False)

    valor_parcelado = forms.DecimalField(label='Valor Parcelado', required=False)
    numero_parcelas = forms.IntegerField(label='Número de Parcelas', required=False)
    data_primeira_parcela = forms.DateField(label='Data da Primeira Parcela', required=False)

    fieldsets = (
        ('Cartão Previamente Cadastrado', {'fields': ('cartao',)}),
        ('Cadastrar Novo Cartão', {'fields': (('bandeira', 'numero_cartao'),)}),
        ('Dados do Pagamento', {'fields': (('valor', 'valor_parcelado'), ('numero_parcelas', 'data_primeira_parcela'))}))

    class Meta:
        submit_label = 'Receber'
        title = 'Recebimento - Cartão de Crédito'


class ReceberCartaoDebitoForm(forms.Form):
    conta = forms.ModelChoiceField(CartaoCredito.objects.none(), label='Cartão', required=False)

    banco = forms.ModelChoiceField(Banco.objects.all(), label='Banco', required=False)
    numero_conta = forms.OneDigitValidationField(label='Conta', required=False)
    numero_agencia = forms.OneDigitValidationField(label='Agência', required=False)

    valor = forms.DecimalField(label='Valor', required=False)

    fieldsets = (
        ('Conta Previamente Cadastrada', {'fields': ('conta',)}),
        ('Cadastrar Nova Conta', {'fields': (('banco', 'numero_conta', 'numero_agencia'),)}),
        ('Dados do Pagamento', {'fields': ('valor',)})
    )

    class Meta:
        submit_label = 'Receber Cartão'
        title = 'Recebimento - Cartão de Débito'


class ReceberTransferenciaBancariaForm(forms.Form):
    conta = forms.ModelChoiceField(ContaBancaria.objects.none(), label='Conta de Destino', required=False)

    valor = forms.DecimalField(label='Valor', required=False)
    data_operacao = forms.DateField(label='Data da Operação', required=False)
    numero_documento = forms.IntegerField(label='Nº do Documento', required=True)

    fieldsets = (
        ('Dados do Pagamento', {'fields': (('conta', 'valor'), ('data_operacao', 'numero_documento'))}),
    )

    class Meta:
        submit_label = 'Receber Cartão'
        title = 'Recebimento - Cartão de Débito'


class AdicionarProdutoTransferencia(forms.Form):
    produto_inicial = forms.ModelChoiceField(Produto.objects.all(), label='Produto Inicial', lazy=True)
    produto_final = forms.ModelChoiceField(Produto.objects.all(), label='Produto Final', lazy=True, required=False)

    class Meta:
        title = 'Adicionar Produto'


class RealizarPagamentoCaixaForm(forms.Form):
    conta = forms.ModelChoiceField(ContaPagar.objects.none(), label='Conta', required=False)
    valor = forms.DecimalField(label='Valor', required=False)

    fieldsets = (
        ('Dados do Pagamento', {'fields': (('conta', 'valor'), )}),
    )

    class Meta:
        submit_label = 'Pagar'
        title = 'Pagamento de Conta'
